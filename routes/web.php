<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//for homepage
Route::get('/','IndexController@index');
Route::get('/listview','IndexController@listview');
Route::get('/registration','IndexController@register');
Route::get('/signin','IndexController@signin');
Route::get('/dashboard','IndexController@dashboard');
//inventory system
Route::get('/admin/transfer/in/{id}','InventoryController@transferIn');
Route::get('/admin/transfer/out/{id}','InventoryController@transferOut');
Route::get('/transfer/save/document','InventoryController@saveDocument');
Route::get('/search/product','InventoryController@searchValidationItem');
Route::get('/add_ppl/save/productList','InventoryController@addToProductList');
Route::get('/getcheck/document','InventoryController@checkDocument');
Route::get('/draft/save','InventoryController@saveDraft');
Route::get('/post/document','InventoryController@postDocument');
Route::get('/transfer/leaving','InventoryController@deleteDocumentRecord');
Route::get('/remove/list/item/','InventoryController@removeItemList');
Route::get('/edit/list/item/','InventoryController@editItemList');
Route::get('/update/doc/date','InventoryController@updateDocumentDate');
Route::get('/bulk/upload/in','InventoryController@bulkUploadIn');
Route::post('/post/bulk/upload/in','InventoryController@postBulkUploadIn');

//reports
Route::get('/reports/search/item','ReportsController@searchItem');
Route::get('/reports/stock/evaluation','ReportsController@stockEvaluation');
Route::get('/reports/sales/analysis','ReportsController@salesAnalysis');
Route::get('/get/platform','ReportsController@getPlatform');
Route::get('/branch/search/autocomplete','ReportsController@branchAutoComplete');
Route::get('/sku/search/autocomplete','ReportsController@SkuAutoComplete');
Route::get('/reports/adjust/product','ReportsController@adjustProduct');
Route::get('/check/exist/list','ReportsController@checkExist');
Route::get('/check/item/quantity','ReportsController@checkQuantity');
Route::get('/adjust/item/quantity','ReportsController@adjustItemQuantity');
// for admin
Route::get('/admin','AdminController@index');

//for bulk upload
Route::post('/bulk/upload','ProductInfoController@uploadBulk');

//for user office info
Route::get('/user/userofficeinfo','UserController@userofficeinfo');
Route::post('/save/userofficeinfo','UserController@saveuserofficeinfo');
Route::post('/update/userofficeinfo','UserController@updateuserofficeinfo');
Route::get('/delete/userofficeinfo/{id}','UserController@removeuserofficeinfo');

//for user account
Route::get('/user/useraccount','UserController@useraccount');
Route::post('/save/account_status','UserController@saveuseraccount');
Route::post('/update/useraccount','UserController@updateuseraccount');
Route::get('/delete/useraccount/{useraccount_id}','UserController@removeuseraccount');

//for user priviliges
Route::get('/user/userpriviliges','UserController@userpriv');
Route::post('/save/userpriviliges','UserController@saveuserpriviliges');
Route::post('/update/userpriviliges','UserController@updateuserpriviliges');
Route::get('/delete/userpriviliges/{id}','UserController@removeuserpriviliges');

//for products
Route::get('/products/product_management','AdminController@productmanagement');


//for product information/specification
Route::get('/productspecs/brand','ProductinfoController@brand');
Route::post('/save/brand','ProductinfoController@savebrand');
Route::post('/update/brand','ProductinfoController@updatebrand');
Route::get('/delete/brand/{id}','ProductinfoController@removebrand');
//for product information/specification
Route::get('/productspecs/band_materials','ProductinfoController@bandmaterials');
Route::post('/save/bandmaterials','ProductinfoController@savebandmaterials');
Route::post('/update/bandmaterials','ProductinfoController@updatebandMaterial');
Route::get('/delete/bandmaterials/{id}','ProductinfoController@removebandMaterial');
//for case material
Route::get('/productspecs/case_materials','ProductinfoController@casematerials');
Route::post('/save/casematerials','ProductinfoController@savecasematerials');
Route::post('/update/casematerials','ProductinfoController@updatecaseMaterial');
Route::get('/delete/casematerials/{id}','ProductinfoController@removecaseMaterial');
//for case thickness
Route::get('/productspecs/case_thickness','ProductinfoController@casethickness');
Route::post('/save/casethickness','ProductinfoController@savecasethickness');
Route::post('/update/casethickness','ProductinfoController@updatecasethickness');
Route::get('/delete/casethickness/{id}','ProductinfoController@removecasethickness');

//for case thickness
Route::get('/productspecs/case_width','ProductinfoController@casewidth');
Route::post('/save/casewidth','ProductinfoController@savecasewidth');
Route::post('/update/casewidth','ProductinfoController@updatecasewidth');
Route::get('/delete/casewidth/{id}','ProductinfoController@removecasewidth');
//for clasp type
Route::get('/productspecs/clasp_type','ProductinfoController@clasptype');
Route::post('/save/clasptype','ProductinfoController@saveclasptype');
Route::post('/update/clasptype','ProductinfoController@updateclasptype');
Route::get('/delete/clasptype/{id}','ProductinfoController@removeclasptype');

//for color
Route::get('/productspecs/color','ProductinfoController@color');
Route::post('/save/color','ProductinfoController@savecolor');
Route::post('/update/color','ProductinfoController@updatecolor');
Route::get('/delete/color/{id}','ProductinfoController@removecolor');

//for face height
Route::get('/productspecs/face_height','ProductinfoController@faceheight');
Route::post('/save/faceheight','ProductinfoController@savefaceheight');
Route::post('/update/faceheight','ProductinfoController@updatefaceheight');
Route::get('/delete/faceheight/{id}','ProductinfoController@removefaceheight');

Route::get('/productspecs/wrist_circumference','ProductinfoController@wristcircumference');
Route::post('/save/wristcircumference','ProductinfoController@savewristcircumference');
Route::post('/update/wristcircumference','ProductinfoController@updatewristcircumference');
Route::get('/delete/wristcircumference/{id}','ProductinfoController@removewristcircumference');

//for water resistance
Route::get('/productspecs/water_resistance','ProductinfoController@waterresistance');
Route::post('/save/waterresistance','ProductinfoController@savewaterresistance');
Route::post('/update/waterresistance','ProductinfoController@updatewaterresistance');
Route::get('/delete/waterresistance/{id}','ProductinfoController@removewaterresistance');

//for Dial
Route::get('/productspecs/dial','ProductinfoController@dial');
Route::post('/save/dial','ProductinfoController@savedial');
Route::post('/update/dial','ProductinfoController@updatedial');
Route::get('/delete/dial/{id}','ProductinfoController@removedial');

//for watch information
Route::get('/productspecs/add_watchinfo','ProductinfoController@addwatchinfo');
Route::get('/productspecs/edit_watchinfo/{id}','ProductinfoController@editwatchinfo');
Route::get('/productspecs/watch_info','ProductinfoController@watchinfo');
Route::post('/save/watchinfo','ProductinfoController@savewatchinfo');
Route::post('/update/watchinfo/','ProductinfoController@updateWatchInfo');
Route::get('/delete/watchinfo/{id}','ProductinfoController@removewatchinfo');

Route::get('/productspecs/image','ProductinfoController@image');
Route::post('/save/image','ProductinfoController@saveimage');
Route::post('/update/image','ProductinfoController@updateimage');
Route::get('/delete/image/{id}','ProductinfoController@removeimage');


Route::get('/productspecs/zip_upload','ProductinfoController@zipupload');
Route::post('/save/zip','ProductinfoController@savezip');
Route::post('/update/zip','ProductinfoController@updatezip');
Route::get('/delete/zip/{id}','ProductinfoController@removezip');

Route::get('/login','LoginController@index');
Route::get('/login/auth','LoginController@loginAuth');
Route::get('/logout','LoginController@logout');

Route::get('/transmittal/area','TransmittalController@area');
Route::post('/save/area','TransmittalController@savearea');
Route::post('/update/area','TransmittalController@updatearea');
Route::get('/delete/area/{id}','TransmittalController@removearea');

Route::get('/transmittal/branch','TransmittalController@branch');
Route::post('/save/branch','TransmittalController@savebranch');
Route::post('/update/branch','TransmittalController@updatebranch');
Route::get('/delete/branch/{id}','TransmittalController@removebranch');

Route::get('/transmittal/branchtype','TransmittalController@branchtype');
Route::post('/save/branchtype','TransmittalController@savebranchtype');
Route::post('/update/branchtype','TransmittalController@updatebranchtype');
Route::get('/delete/branchtype/{id}','TransmittalController@removebranchtype');

Route::get('/transmittal/chronotype','TransmittalController@ChronoType');
Route::post('/save/chronotype','TransmittalController@saveChronoType');
Route::post('/update/chronotype','TransmittalController@updateChronoType');
Route::get('/delete/chronotype/{id}','TransmittalController@removeChronoType');

Route::get('/transmittal/promo','TransmittalController@Promo');
Route::post('/save/promo','TransmittalController@savePromo');
Route::post('/update/promo','TransmittalController@updatePromo');
Route::get('/delete/promo/{id}','TransmittalController@removePromo');

Route::get('/transmittal/transmittal_info','TransmittalController@transmittal');
Route::get('/transmittal/add_transinfo','TransmittalController@addTransInfo');
Route::get('/transmittal/edit_transinfo/{id}','TransmittalController@editTransInfo');
Route::post('/save/transmittal_info','TransmittalController@saveTransInfo');
Route::post('/update/transmittal_info','TransmittalController@updateTransInfo');
Route::get('/delete/transmittal_info/{id}','TransmittalController@removeTransInfo');

Route::get('/transmittal/print_preview','TransmittalController@printPreview');

Route::get('/transaction/platform','TransactionController@platform');
Route::post('/save/platform','TransactionController@saveplatform');
Route::post('/update/platform','TransactionController@updateplatform');
Route::get('/delete/platform/{id}','TransactionController@removeplatform');

Route::get('/transaction/product_transaction','TransactionController@product_transaction');
Route::post('/save/product_transaction','TransactionController@savetransaction');

Route::get('/transaction/watchlist','TransactionController@watchlist');
