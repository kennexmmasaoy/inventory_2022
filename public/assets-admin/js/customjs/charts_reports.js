// REPORTS CHARTS
    // BAR CHART
    //-----//
    var ctx = document.getElementById('revenue_comparison_chart').getContext('2d');
    var data = {
      labels: ["2016", "Q4*13", "Dec 13"],
      datasets: [{
        label: "Actual",
        backgroundColor: "rgb(0, 97, 255)",
        data: [1223, 921, 1052,]
      }, {
        label: "Budget",
        backgroundColor: "rgb(63, 136, 255)",
        data: [3022, 525, 6252,]
      }, {
       
      }]
    };
    var barChartVcl = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
        legend: {
            display: false,
        },
        barValueSpacing: 20,
        scales: {
          yAxes: [{
            ticks: {
              min: 0,
            }
          }]
        }
      }
    });
  //-----//
  //-----//
    var ctx = document.getElementById('quantity_comparison_chart').getContext('2d');
    var data = {
      labels: ["2016", "Q4*13", "Dec 13"],
      datasets: [{
        label: "Actual",
        backgroundColor: "rgb(0, 97, 255)",
        data: [1203, 493, 454,]
      }, {
        label: "Budget",
        backgroundColor: "rgb(63, 136, 255)",
        data: [1123, 564, 342,]
      }, {
       
      }]
    };
    var barChartVcl = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
        legend: {
            display: false,
        },
        barValueSpacing: 20,
        scales: {
          yAxes: [{
            ticks: {
              min: 0,
            }
          }]
        }
      }
    });
  //-----//
  //-----//
    var ctx = document.getElementById('top_4_markets_chart').getContext('2d');
    var data = {
      labels: ["Kyoto", "Los Angeles", "NYC", "Manila",],
      datasets: [{
        label: "Visits",
        backgroundColor: "rgb(0, 97, 255)",
        data: [1223, 921, 1052, 1189,]
      }]  
    };
    var barChartVcl = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
        legend: {
            display: false,
        },
      }
    });
    //-----//
    //-----//
    var ctx = document.getElementById('top_4_products_chart').getContext('2d');
    var data = {
      labels: ["Watches", "Electronics", "Health & Living", "Apparels",],
      datasets: [{
        label: "Visits",
        backgroundColor: "rgb(0, 97, 255)",
        data: [1223, 921, 1052, 1189,]
      }]  
    };
    var barChartVcl = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
        legend: {
            display: false,
        },
      }
    });
    //-----//
    // END BAR CHART
    // PIE CHART
    //-----//
    var ctx = document.getElementById("product_sales_mix_chart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ["Food And Bev", "Apparels", "Electronics",],
        datasets: [
            {
                backgroundColor: ["#2ecc71","#3498db","#95a5a6",],
                data: [121, 192, 333,]
            }
        ]
      },
      options: {
            legend: {
                display: false,
                position: 'bottom',
            }
        }
    });
    //-----//
    //-----//
    var ctx = document.getElementById("regional_sales_mix").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: ["Americas", "APAC", "SEA",],
        datasets: [
            {
                backgroundColor: ["#2ecc71","#3498db","#95a5a6",],
                data: [59612658, 27687429, 25642132,]
            }
        ]
      },
      options: {
            legend: {
                display: false,
                position: 'bottom',
            }
        }
    });
    //-----//
    // END PIE CHART
// END REPORTS CHARTS