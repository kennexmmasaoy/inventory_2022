  // DASHBOARD CHARTS  
    // START BAR CHART
    var ctx = document.getElementById('bar_chart_vcl').getContext('2d');
    var data = {
      labels: ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      datasets: [{
        label: "Visits",
        backgroundColor: "rgb(0, 97, 255)",
        data: [1223, 921, 1052, 1189, 12332, 802, 654, 545, 302, 519, 702, 1023]
      }, {
        label: "Customers",
        backgroundColor: "rgb(63, 136, 255)",
        data: [3022, 525, 6252, 572, 620, 783, 853, 953, 1023, 1032, 800, 902]
      }, {
        label: "Leads",
        backgroundColor: "rgb(104, 161, 255)",
        data: [703, 402, 422, 2601, 426, 102, 225, 325, 504, 592, 102, 82]
      }]
    };
    var barChartVcl = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
        barValueSpacing: 20,
        scales: {
          yAxes: [{
            ticks: {
              min: 0,
            }
          }]
        }
      }
    });
    // END BAR CHART
    // START LINE CHART
    //-----//
    var options = {
      type: 'line',
      data: {
        labels: ["8:00 AM", "10:00 AM", "12:00 PM", "2:00 PM", "4:00 PM",],
        datasets: [
            {
                label: '# of Votes',
                data: [32001, 345121, 35212, 36214, 35623,],
                backgroundColor:"rgb(82, 99, 127)",
                borderWidth: 1
            } 
        ]
      },
      options: {
        elements: {
            line: {
                tension: 0,
            }
        },
        legend: {
            display: false,
        },
        scales: {
            yAxes: [{
            ticks: {
                reverse: false
            }
          }]
        }
      }
    }
    var ctx = document.getElementById('line_chart_dsp').getContext('2d');
    new Chart(ctx, options);
    //-----//
    //-----//
    var options = {
      type: 'line',
      data: {
        labels: ["08/01", "08/06", "08/11", "08/21", "08/26",],
        datasets: [
            {
                label: false,
                data: [1123328, 1133124, 1140512, 1120201, 1133425,],
                backgroundColor: "rgb(82, 122, 127)",
                 borderWidth: 1
            } 
        ]
      },
      options: {
        elements: {
            line: {
                tension: 0,
            }
        },
        legend: {
            display: false,
        },
        scales: {
            yAxes: [{
            ticks: {
                reverse: false
            }
          }]
        }
      }
    }
    var ctx = document.getElementById('line_chart_msp').getContext('2d');
    new Chart(ctx, options);
    //-----//
    // END LINE CHART
    // START DOUGHNUT
    var ctx = document.getElementById("doughnut_chart_sbm").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ["Uni", "HEA", "MOA", "Casio", "RQ", "SR Foods",],
        datasets: [
            {
                backgroundColor: ["#2ecc71","#3498db","#95a5a6","#9b59b6","#f1c40f","#e74c3c","#34495e"],
                data: [121, 192, 333, 217, 238, 224,]
            }
        ]
      },
      options: {
            legend: {
                display: true,
                position: 'left',
            }
        }
    });
    // END DOUGHTNUT
    // SPEEDOMETER
    //-----//
    var ctx = document.getElementById("speedometer_chart_ftd");
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["1","2","3"],
            datasets: [{
                label: '# of Votes',
                data: [5002000, 9520000, 10002000,],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
                display: true,
                position: 'none',
            },
            rotation: 1 * Math.PI,
            circumference: 1.5 * Math.PI
            
        }
    });
    //-----//
    //-----//
    var ctx = document.getElementById("speedometer_chart_fy");
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["1","2","3"],
            datasets: [{
                label: '# of Votes',
                data: [122, 191, 323,],
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(75, 192, 192, 1)',
        
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
                display: true,
                position: 'none',
            },
            rotation: 1 * Math.PI,
            circumference: 1.5 * Math.PI
            
        }
    });
    //-----//
    // END SPEEDOMETER
    // STACKED BAR CHART
    Chart.defaults.groupableHBar = Chart.helpers.clone(Chart.defaults.horizontalBar);

    Chart.controllers.groupableHBar = Chart.controllers.horizontalBar.extend({
        calculateBarY: function(index, datasetIndex, ruler) {
            var me = this;
            var meta = me.getMeta();
            var yScale = me.getScaleForId(meta.yAxisID);
            var barIndex = me.getBarIndex(datasetIndex);
            var topTick = yScale.getPixelForValue(null, index, datasetIndex, me.chart.isCombo);
            topTick -= me.chart.isCombo ? (ruler.tickHeight / 2) : 0;
            var stackIndex = this.getMeta().stackIndex;

            if (yScale.options.stacked) {
                if(ruler.datasetCount>1) {
                    var spBar=ruler.categorySpacing/ruler.datasetCount;
                    var h=me.calculateBarHeight(ruler);
                    
                    return topTick + (((ruler.categoryHeight - h) / 2)+ruler.categorySpacing-spBar/2)+(h+spBar)*stackIndex;
                }
                return topTick + (ruler.categoryHeight / 2) + ruler.categorySpacing;
            }

            return topTick +
                (ruler.barHeight / 2) +
                ruler.categorySpacing +
                (ruler.barHeight * barIndex) +
                (ruler.barSpacing / 2) +
                (ruler.barSpacing * barIndex);
        },
        calculateBarHeight: function(ruler) {
            var returned=0;
            var me = this;
            var yScale = me.getScaleForId(me.getMeta().yAxisID);
            if (yScale.options.barThickness) {
                returned = yScale.options.barThickness;
            }
            else {
                returned= yScale.options.stacked ? ruler.categoryHeight : ruler.barHeight;
            }
            if(ruler.datasetCount>1) {
                returned=returned/ruler.datasetCount;
            }
            return returned;
        },
        getBarCount: function () {
            var stacks = [];

            // put the stack index in the dataset meta
            Chart.helpers.each(this.chart.data.datasets, function (dataset, datasetIndex) {
                var meta = this.chart.getDatasetMeta(datasetIndex);
                if (meta.bar && this.chart.isDatasetVisible(datasetIndex)) {
                    var stackIndex = stacks.indexOf(dataset.stack);
                    if (stackIndex === -1) {
                        stackIndex = stacks.length;
                        stacks.push(dataset.stack);
                    }
                    meta.stackIndex = stackIndex;
                }
            }, this);

            this.getMeta().stacks = stacks;

            return stacks.length;
        }
    });




    var data = {
      labels: ["",],
      datasets: [
        {
          label: "Minimum",
          backgroundColor: "rgba(255, 0, 0, 1)",
          data: [300,],
          stack: 1,
          xAxisID: 'x-axis-0',
          yAxisID: 'y-axis-0'
        },
        {
          label: "Meeting",
          backgroundColor: "rgba(255, 255, 0, 1)",
          data: [200,],
          stack: 1,
          xAxisID: 'x-axis-0',
          yAxisID: 'y-axis-0'
        },
        {
          label: "Exceeding",
          backgroundColor: "rgba(0, 255, 255, 1)",
          data: [100,],
          stack: 1,
          xAxisID: 'x-axis-0',
          yAxisID: 'y-axis-0'
        },
      ]
    };

    var ctx = document.getElementById("stacked_chart_sales_forecast").getContext("2d");
    new Chart(ctx, {
      type: 'groupableHBar',
      data: data,
      options: {
        legend: {
            display: false,
        },
        scales: {
          yAxes: [{
            stacked: true,
            type: 'category',
            id: 'y-axis-0'
          }],
          xAxes: [{
            stacked: true,
            type: 'linear',
            ticks: {
                min: 0,
                max: 600,

              beginAtZero:true
            },
            gridLines: {
              display: false,
              drawTicks: false,
            },
            id: 'x-axis-0'
          },
          {
            stacked: true,
            position: 'top',
            type: 'linear',
            ticks: {
              beginAtZero:true
            },
            id: 'x-axis-1',
            gridLines: {
              display: true,
              drawTicks: true,
            },
            display: false
          }]
        }
      }
    });
    // END STACKED BAR CHART
// END DASHBOARD CHARTS