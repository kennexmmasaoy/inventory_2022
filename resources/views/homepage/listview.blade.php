@extends('layouts.homepage-default')
@section('content')
<!-- for navbar -->
<!-- end of navbar -->
<div class="container">
  <div class="col-xs-12">
    <div class="col-lg-4 pull-right">
      {!! Form::open(['method'=>'GET','url'=>'/listview','role'=>'search'])  !!}
      <div class="input-group custom-search-form">
          <input type="text" class="form-control" name="search" placeholder="Search...">
          <span class="input-group-btn">
              <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
              </button>
          </span>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div class="col-xs-12"><hr>
    <div class="col-md-2 pull-right">
      <p class="pull-left">View As : &nbsp;</p>
      <a href="{{url('/')}}" ><i class="color_code fa fa-th" style="margin-top:5px;"></i></a>
      <a href="{{url('/listview')}}" ><i class="color_code fa fa-list"></i></a>
    </div>
  </div>
  <div class="row"><br><hr>
    @foreach($info_list as $row)
      <div class="col-sm-12">
        <h3>{{$row->product_name}}</h3>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <center>
                  <?php  $image = App\Http\Model\ImageModel::where('id',$row->image_id)->select('savename')->first();  ?>
                <img src="{{asset('/upload/'. $image['savename'])}}" style="height:233px;max-width:233px"><br><br>
              </center>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p><b>SKU:</b><span>&nbsp;sku</span></p>
                <p><b>PRICE:</b><span>&nbsp;{{$row->price}}</span></p>
                <p><b>DESCRIPTION:</b><span>&nbsp;description</span></p>
                <p><b>COLOR:</b><span>&nbsp;({{$row->color_name}})({{$row->color_hex}})({{$row->color_rgb}})</span></p>
                <p><b>DIAL:</b><span>&nbsp;{{$row->dial_name}}</span></p>
                <p><b>CASE MATERIAL:</b><span>&nbsp;{{$row->material}}</span></p>
                <p><b>FACE HEIGHT:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <p><b>CASE THICKNESS:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                <p><b>MAX WRIST CIRCUMFERENCE:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                <p><b>CLASP TYPE:</b><span>&nbsp;{{$row->type}}</span></p>
                <p><b>WATER RESISTANCE:</b><span>&nbsp;{{$row->depht}}</span></p>
                <p><b>WARRANTY:</b><span>&nbsp;{{$row->warranty}}</span></p>
                <p><b>OTHER FEATURES:</b><span>&nbsp;{{$row->other_features}}</span></p>
                <p><b>CASE WIDTH:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
              </div>
            </div>
      </div>
      <div class="clear"></div>
    @endforeach
</div>
<script type="text/javascript">
  $(".show-more a").on("click", function() {
    var $this = $(this); 
    var $content = $this.parent().prev("div.content");
    var linkText = $this.text().toUpperCase();    
    
    if(linkText === "SHOW MORE"){
        linkText = "Show less";
        $content.switchClass("hideContent", "showContent", 400);
    } else {
        linkText = "Show more";
        $content.switchClass("showContent", "hideContent", 400);
    };

    $this.text(linkText);
});
</script>
<script type="text/javascript" src="{{asset('assets/js/index.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
@stop