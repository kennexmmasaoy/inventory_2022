@extends('layouts.homepage-default')
@section('content')
<!-- for navbar -->
<!-- end of navbar -->
<div class="container">
  <div class="col-xs-12">
    <div class="col-lg-4 pull-right">
      {!! Form::open(['method'=>'GET','url'=>'/','role'=>'search'])  !!}
      <div class="input-group custom-search-form">
          <input type="text" class="form-control" name="search" placeholder="Search...">
          <span class="input-group-btn">
              <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
              </button>
          </span>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  <div class="col-xs-12"><br>
    <div class="input-group col-xs-6">
      <div class="input-group-prepend">
        <label class="input-group-text" for="inputGroupSelect01">Sort By:</label>
      </div>
      <select class="custom-select" id="inputGroupSelect01">
        <option selected>Price to Low</option>
        <option value="1">Low to High</option>
      </select>
    </div>
    <div class="row col-md-2 pull-right" style="margin-top: -20px;">
      <p class="pull-left">View As : &nbsp;</p>
      <a href="{{url('/')}}" ><i class="color_code fa fa-th" style="margin-top:5px;"></i></a>
      <a href="{{url('/listview')}}" ><i class="color_code fa fa-list"></i></a>
    </div>
  </div>
  <div class="row"><br><hr>
    
      @foreach($info_list as $row)
      <div class="col-sm-3" style="position:relative;">
            <h3>{{$row->product_name}}</h3>
            <center>
                <?php  $image = App\Http\Model\ImageModel::where('id',$row->image_id)->select('savename')->first();  ?>
                <img src="{{asset('/upload/'. $image['savename'])}}" style="height:233px;max-width:100%;">
            </center>
            <p><b>SKU:</b><span>&nbsp;{{$row->sku}}</span></p>
            <p><b>PRICE:</b><span>&nbsp;{{$row->price}}</span></p>
            <p><b>DESCRIPTION:</b><span>&nbsp;{{$row->product_description}}</span></p>
            <a style="cursor:pointer;" data-toggle="modal" data-target="#myModal{{$row->info_id}}">See More Details...</a>
            
      </div><div class="clear"></div>
      <!-- Modal -->
      <div id="myModal{{$row->info_id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{$row->sku}}</h4>
            </div>
            <div class="modal-body">
              <div class="col-xs-12">
                <div class="col-xs-6">
                  <h3>{{$row->product_name}}</h3>
                  <center>
                      <?php  $image = App\Http\Model\ImageModel::where('id',$row->image_id)->select('savename')->first();  ?>
                      <img src="{{asset('/upload/'. $image['savename'])}}" style="min-height:233px;;max-width:100%;">
                  </center>
                  <p><b>SKU:</b><span>&nbsp;{{$row->sku}}</span></p>
                  <p><b>PRICE:</b><span>&nbsp;{{$row->price}}</span></p>
                  <p><b>DESCRIPTION:</b><span>&nbsp;{{$row->product_description}}</span></p>
                </div>
                <div class="col-xs-6">
                  <p><b>COLOR:</b><span>&nbsp;({{$row->color_name}})({{$row->color_hex}})({{$row->color_rgb}})</span></p>
                  <p><b>DIAL:</b><span>&nbsp;{{$row->dial_name}}</span></p>
                  <p><b>CASE MATERIAL:</b><span>&nbsp;{{$row->material}}</span></p>
                  <p><b>FACE HEIGHT:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                  <p><b>CASE WIDTH:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                  <p><b>CASE THICKNESS:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                  <p><b>MAX WRIST CIRCUMFERENCE:</b><span>&nbsp;{{$row->size}}{{$row->lenght_indicator}}</span></p>
                  <p><b>CLASP TYPE:</b><span>&nbsp;{{$row->type}}</span></p>
                  <p><b>WATER RESISTANCE:</b><span>&nbsp;{{$row->depht}}</span></p>
                  <p><b>WARRANTY:</b><span>&nbsp;{{$row->warranty}}</span></p>
                  <p><b>OTHER FEATURES:</b><span>&nbsp;{{$row->other_features}}</span></p>
                </div>
              </div><div class="clear"></div>
              
            </div><br><br><br><br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
      <!-- <input type="checkbox" id="check_id{{$row->info_id}}">
      <label for="check_id{{$row->info_id}}"></label>
      <ul class="displaybox"> -->
      @endforeach
    
  </div>
</div>
<script type="text/javascript" src="{{asset('assets/js/index.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
@stop