@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Product Transaction</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div>
      {!! Form::close() !!}
			<div class="clear"></div>
			<form method="post" action="{{action('TransactionController@savetransaction')}}">
				<div class="col-md-6 well" style="color:#000;">
						<div class="form-group">
							
	  						<label for="">Platform Name:</label>
	  						<select class="form-control" name="platform_id" id="platform_id">
	  							<option value="0">Select Platform</option>
	  							@foreach($plat_info as $row)
					 			<option value="{{$row->id}}">{{$row->platform_name}}</option>
							    @endforeach
					  		</select>
					  		
						</div>
						<div class="form-group">
	  						<label for="">Action (In/Out):</label>
	  						<select class="form-control" name="type" id="type">
					 			<option value="1">In Item</option>
							    <option value="2">Out Item</option>
					  		</select>
						</div>
						<div class="form-group">
	  						<label for="fatures">quantity:</label>
	 						<input type="text" class="form-control" id="transaction_qty" name="transaction_qty" required>
						</div>
						{!! csrf_field() !!}
						<input type="hidden" name="id" value="" />
						<input name="submit" type="submit" class="btn btn-success pull-right" value="Save">
					</div>
				</div>
			</form>
			<div class="col-md-6">
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of transaction
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Platform Name</th>
										<th class="text-center">Action Type</th>
										<th class="text-center">Product Quantity</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($transac_info as $item)
									<tr> 
										<td class="text-center">{{$item->transac_id}}</td>
										<td class="text-center">{{$item->platform_name}}</td>
										<td class="text-center">{{$item->type}}</td>
										<td class="text-center">{{$item->transaction_qty}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop