@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Platform</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<divclass="col-xs-12">
				<button type="button" class="btn btn-info btn-xs" 
				data-toggle="modal" data-target="#myModal">Add Platform</button>
			</div>
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  	<div class="modal-dialog">

			    	<!-- Modal content-->
				    <div class="modal-content">
					    <form method="post" action="{{action('TransactionController@saveplatform')}}" enctype="multipart/form-data">
					      	<div class="modal-header">
					        	<button type="button" class="close" data-dismiss="modal">
					        		&times;</button>
					        	<h4 class="modal-title">Add Platform</h4>
					      	</div>
					      	<div class="modal-body">
					        	
								<div class="col-md-12 well" style="color:#000;">
									
									<div class="form-group">
				  						<label for="usr" style="color:#000;">Platform Name:</label>
				 						<input type="text" style="color:#000;" class="form-control" id="platform_name" name="platform_name" required>
									</div>
									
									{!! csrf_field() !!}
									<input type="hidden" name="id" value="" />
								</div>
						    </div>
					      	<div class="modal-footer">
					        	<input name="submit" type="submit" class="btn btn-success pull-right" value="Save">
					      	</div>
					    </form>
				    </div>

			  	</div>
			</div>
			<div class="clear"></div>
			<div class="col-md-12"><br><hr/>
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of Platform
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Platform Name</th>
										<th class="text-center">Options</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($plat_list as $item)
									<tr> 
										<td class="text-center">{{$item->id}}</td>
										<td class="text-center">{{$item->platform_name}}</td>
										<td>
											<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal{{$item->id}}">Edit</button>
											<!-- Modal -->
											<div id="myModal{{$item->id}}" class="modal fade" role="dialog">
											  	<div class="modal-dialog">

											    	<!-- Modal content-->
												    <div class="modal-content">
													    <form method="post" action="{{action('TransactionController@updateplatform')}}" enctype="multipart/form-data">
													      	<div class="modal-header">
													        	<button type="button" class="close" data-dismiss="modal">
													        		&times;</button>
													        	<h4 class="modal-title">Update Platform</h4>
													      	</div>
													      	<div class="modal-body">
													        	
																<div class="col-md-12" style="color:#000;">
																	<div class="form-group">
												  						<label>Platform Name:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="platform_name" name="platform_name"
												 						value="{{$item->platform_name}}" required>
																	</div>
																	
																	<input type="hidden" name="id" value="{{$item->id}}">
																	{!! csrf_field() !!}
																</div>
														    </div>
													      	<div class="modal-footer">
													        	<button type="submit" class="btn btn-success pull-right">
													        	<i class="fa fa-floppy-o" aria-hidden="true">
													        	</i>&nbsp;&nbsp;Save</button>
													      	</div>
													    </form>
												    </div>

											  	</div>
											</div>
												
            									<!-- <a href="#" class="btn btn-primary btn-xs">Details</a> -->
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop