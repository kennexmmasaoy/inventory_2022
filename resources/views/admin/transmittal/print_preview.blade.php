<!DOCTYPE html>
<html lang="en">
<head>
  <title>Print Preview</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  
<div class="container"><br>
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
      <div class="col-xs-12" style="border: 1px solid rgba(0, 0, 0, 0.3);">
        <h4 class="text-center">Transmittal Form</h4>
        <label>Date:</label><span>date</span>
        <h5 class="text-center">Assorted Signagess</h5>
        <label>To:</label><span>branch name</span><br>
        <label>From:</label><span>department</span><br>
        <h6 class="text-warning text-center">SINTRA SIGNAGES</h6>
        <small>Please acknowledge receipt of the following item(s):</small>
        <div class="col-xs-12"><br>
          <table class="table">
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>10%</td>
                <td>1</td>
                <td>@pcs</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>20%</td>
                <td>1</td>
                <td>pcs</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>buy 1 free 1</td>
                <td>2</td>
                <td>pcs</td>
              </tr>
            </tbody>
          </table>
          <div class="col-xs-12">
            <label class="pull-right">Total:&nbsp;&nbsp;<span>21 items</span></label>
          </div>
        </div>
        <div class="clear"></div>
        <label>Prepared by:</label><span>name</span><br>
        <label>Received by:</label><span>signature and date</span><br>
      </div>
    </div>
    <div class="col-sm-5">
      <div class="col-xs-12" style="border: 1px solid rgba(0, 0, 0, 0.3);">
        <h4 class="text-center">Transmittal Form</h4>
        <label>Date:</label><span>date</span>
        <h5 class="text-center">Assorted Signagess</h5>
        <label>To:</label><span>branch name</span><br>
        <label>From:</label><span>department</span><br>
        <h6 class="text-warning text-center">SINTRA SIGNAGES</h6>
        <small>Please acknowledge receipt of the following item(s):</small>
        <div class="col-xs-12"><br>
          <table class="table">
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>10%</td>
                <td>1</td>
                <td>@pcs</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>20%</td>
                <td>1</td>
                <td>pcs</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>buy 1 free 1</td>
                <td>2</td>
                <td>pcs</td>
              </tr>
            </tbody>
          </table>
          <div class="col-xs-12">
            <label class="pull-right">Total:&nbsp;&nbsp;<span>21 items</span></label>
          </div>
        </div>
        <div class="clear"></div>
        <label>Prepared by:</label><span>name</span><br>
        <label>Received by:</label><span>signature and date</span><br>
      </div>
    </div>
    <div class="col-sm-1"></div>
</div>

</body>
</html>
