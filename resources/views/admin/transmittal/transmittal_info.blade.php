@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Transmittal Information</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div class="col-xs-4 pull-right">
				{!! Form::open(['method'=>'GET','url'=>'/transmittal/transmittal_info','role'=>'search'])  !!}
			      <div class="input-group custom-search-form">
			          <input type="text" class="form-control" name="search" placeholder="Search...">
			          <span class="input-group-btn">
			              <button class="btn btn-default-sm" type="submit">
			                  <i class="fa fa-search"></i>
			              </button>
			          </span>
			      </div>
			      {!! Form::close() !!}
			</div>
			<div class="col-md-6">
				<div class="btn-group">
					<a href="{{url('/transmittal/add_transinfo')}}" type="button" class="btn btn-info">
						<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add
					</a>
				</div>
			</div>
			<div class="clear"></div>
			<div class="col-md-12"><hr>
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of Transmittal Information
						</h3>
						<div class="btn-group pull-right" style="margin-top:-35px;">
							<a href="{{url('/transmittal/print_preview')}}" type="button" class="btn btn-success">
								<i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print Preview
							</a>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Area</th>
										<th class="text-center">Out of Delivery</th>
										<th class="text-center">Location</th>
										<th class="text-center">Branch</th>
										<th class="text-center">Branch Type</th>
										<th class="text-center">Chrono Type</th>
										<th class="text-center">Promo</th>
										<th class="text-center">Major Tarpaulin</th>
										<th class="text-center">Generic</th>
										<th class="text-center">Paper Signage</th>
										<th class="text-center">Secondary Taprpaulin</th>
										<th class="text-center">Generic</th>
										<th class="text-center">Paper Signage</th>
										<th class="text-center">Options</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($trans_info as $item)
									<tr> 
										<td class="text-center">{{$item->trans_id}}</td>
										<td class="text-center">{{$item->area_number}}-{{$item->area_name}}</td>
										<td class="text-center">{{$item->out_of_delivery}}</td>
										<td class="text-center">{{$item->location}}</td>
										<td class="text-center">{{$item->branch_name}}</td>
										<td class="text-center">{{$item->branch_type_name}}</td>
										<td class="text-center">{{$item->chrono_type_name}}</td>
										<td class="text-center">{{$item->promo_name}}</td>
										<td class="text-center">{{$item->m_tarpaulin}}</td>
										<td class="text-center">{{$item->m_generic}}</td>
										<td class="text-center">{{$item->m_paper_signage}}</td>
										<td class="text-center">{{$item->s_tarpaulin}}</td>
										<td class="text-center">{{$item->s_generic}}</td>
										<td class="text-center">{{$item->s_paper_signage}}</td>
										<td class="text-center">
											<a href="{{url('/transmittal/edit_transinfo/'.$item->trans_id)}}" type="button" class="btn btn-info btn-xs">Edit</button>
											<input name="_method" type="hidden" value="DELETE">
        									<a href="{{url('delete/transmittal_info/'.$item->trans_id)}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								{{$trans_info->render()}}
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop