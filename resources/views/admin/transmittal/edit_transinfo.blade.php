@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Add Watch Information</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<div class="clear"></div>
			<form method="post" action="{{action('TransmittalController@updateTransInfo')}}"  enctype="multipart/form-data">
				<div class="col-md-12 well">
						<div class="form-group col-xs-6">
	  						<label for="">Area:</label>
	  						<select class="form-control" name="area" id="area">
					 			<option value="0">Select Area</option>
					 			@foreach($area_info as $row)
							    <option value="{{$row->id}}" @if($item->area == $row->id) selected @endif>
							    	{{$row->area_number}}-{{$row->area_name}}
							    </option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Out of Delivery:(Day of the week)</label>
	 						<input type="text" class="form-control" id="out_of_delivery" value="{{$item->out_of_delivery}}" name="out_of_delivery" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Location:(ex:Sm Manila)</label>
	 						<input type="text" class="form-control" id="location" value="{{$item->location}}" name="location" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Branch:</label>
	 						<select class="form-control" name="branch" id="branch">
					 			<option value="0">Select Branch</option>
					 			@foreach($branch_info as $row)
							    <option value="{{$row->id}}" @if($item->branch == $row->id) selected @endif>{{$row->branch_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Branch Type:</label>
	  						<select class="form-control" name="branch_type" id="branch_type">
					 			<option value="0">Select Branch Type:</option>
					 			@foreach($branchtype_info as $row)
							    <option value="{{$row->id}}"  @if($item->branch_type == $row->id) selected @endif>{{$row->branch_type_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Chrono Type:</label>
	  						<select class="form-control" name="chrono_type" id="chrono_type">
					 			<option value="0">Select Chrono Type:</option>
					 			@foreach($chrono_info as $row)
							    <option value="{{$row->id}}" @if($item->chrono_type == $row->id) selected @endif>{{$row->chrono_type_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-12">
	  						<label for="">Promo Type:</label>
	  						<select class="form-control" name="promo_type" id="promo_type">
					 			<option value="0">Select Promo Type:</option>
					 			@foreach($promo_info as $row)
							    <option value="{{$row->id}}" @if($item->promo_name == $row->id) selected @endif>{{$row->promo_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="col-xs-6">
							<div class="form-group col-xs-12">
								<label for="">Major Promo:</label>
							</div>
							<div class="form-group col-xs-12">
		  						<label for="">Tarpaulin:(size ex:16 by 20)</label>
		 						<input type="text" class="form-control" value="{{$item->m_tarpaulin}}" id="m_tarpaulin" name="m_tarpaulin" required>
							</div>
							<div class="form-group col-xs-12">
		  						<label for="">Generic:(pc/s)</label>
		 						<input type="text" class="form-control" value="{{$item->m_generic}}" id="m_generic" name="m_generic" required>
							</div>
						
							<div class="form-group col-xs-12">
		  						<label for="">Paper Signage:(pc/s)</label>
		 						<input type="text" class="form-control" value="{{$item->m_paper_signage}}" id="m_paper_signage" name="m_paper_signage" required>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group col-xs-12">
								<label for="">Secondary Promo:</label>
							</div>
							<div class="form-group col-xs-12">
		  						<label for="">Tarpaulin:(size ex:16 by 20)</label>
		 						<input type="text" class="form-control" value="{{$item->s_tarpaulin}}" id="s_tarpaulin" name="s_tarpaulin" required>
							</div>
							<div class="form-group col-xs-12">
		  						<label for="">Generic:(pc/s)</label>
		 						<input type="text" class="form-control" value="{{$item->s_generic}}" id="s_generic" name="s_generic" required>
							</div>
							<div class="form-group col-xs-12">
		  						<label for="">Paper Signage:(pc/s)</label>
		 						<input type="text" class="form-control" value="{{$item->s_paper_signage}}" id="s_paper_signage" name="s_paper_signage" required>
							</div>
							{!! csrf_field() !!}
						</div>
						<input type="hidden" name="id" value="{{$item->id}}"/>
						<input type="submit" class="btn btn-success" value="Save">
						<a href="{{url('/transmittal/transmittal_info')}}" type="button" class="btn btn-danger">
							<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
					</div>
				
			</form>			
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop