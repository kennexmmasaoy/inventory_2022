@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>User Accounts</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div class="clear"></div>
			<form method="post" action="{{action('UserController@saveuseraccount')}}">
				<div class="col-md-6 well" style="color:#000;">
					<div class="form-group">
  						<label for="usr" style="color:#000;">User Name:</label>
 						<input type="text" style="color:#000;" class="form-control" id="username" 
 						name="username" required>
					</div>
					<div class="form-group">
  						<label for="usr" style="color:#000;">Password:</label>
 						<input type="text" style="color:#000;" class="form-control" id="password" 
 						name="password" required>
					</div>
					<div class="form-group">
  						<label for="">User Privlages:</label>
 						<select class="form-control" name="user_priv" id="user_priv">
				 			<option>Select User Privilages</option>
				 			@foreach($userpriv as $row)
						    <option value="{{$row->id}}">{{$row->p_name}}</option>
						    @endforeach
				  		</select>
					</div>
					{!! csrf_field() !!}
					<input type="hidden" name="id" value="" />
					<input name="submit" type="submit" class="btn btn-success pull-right" value="Save">
				</div>
			</form>
			<div class="col-md-6">
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of User Accounts
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Username</th>
										<th class="text-center">Password</th>
										<th class="text-center">User Privilages</th>
										<th class="text-center">Option</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($account_list as $item)
									<tr> 
										<td class="text-center">{{$item->useraccount_id}}</td>
										<td class="text-center">{{$item->username}}</td>
										<td class="text-center">{{$item->password}}</td>
										<td class="text-center">{{$item->p_name}}</td>
										<td class="text-center">
											<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal{{$item->id}}">Edit</button>
											<!-- Modal -->
											<div id="myModal{{$item->id}}" class="modal fade" role="dialog">
											  	<div class="modal-dialog">

											    	<!-- Modal content-->
												    <div class="modal-content">
													    <form method="post" action="{{action('UserController@updateuserofficeinfo')}}">
													      	<div class="modal-header">
													        	<button type="button" class="close" data-dismiss="modal">
													        		&times;</button>
													        	<h4 class="modal-title">Update User Office Information</h4>
													      	</div>
													      	<div class="modal-body">
													        	
																<div class="col-md-12" style="color:#000;">
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">User Name:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="username" 
												 						name="username" value="{{$item->username}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Password:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="password" 
												 						name="password" value="{{$item->password}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="">User Privlages:</label>
												 						<select class="form-control" name="user_priv" id="user_priv">
																 			<option>Select User Privilages</option>
																 			@foreach($userpriv as $row)
																		    <option value="{{$row->id}}">
																		    @if($item->user_priv == $row->id) selected @endif{{$row->p_name}}
																			</option>
																		    @endforeach
																  		</select>
																	</div>
																	<input type="hidden" name="id" value="{{$item->id}}">
																	{!! csrf_field() !!}
																</div>
														    </div>
													      	<div class="modal-footer">
													        	<button type="submit" class="btn btn-success pull-right">
													        	<i class="fa fa-floppy-o" aria-hidden="true">
													        	</i>&nbsp;&nbsp;Save</button>
													      	</div>
													    </form>
												    </div>

											  	</div>
											</div>
												<input name="_method" type="hidden" value="DELETE">
            									<a href="{{url('delete/useraccount/'. $item->useraccount_id)}}">Delete</a>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop