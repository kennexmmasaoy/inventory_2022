@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>User Office Information</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div class="clear"></div>
			<form method="post" action="{{action('UserController@saveuserofficeinfo')}}">
				<div class="col-md-6 well" style="color:#000;">
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Office ID Number:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="office_id_number" 
	 						name="office_id_number" required>
						</div>
						<div class="form-group">
	  						<label for="usr" style="color:#000;">First Name:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="firstname" name="firstname" required>
						</div>
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Last Name:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="lastname" name="lastname" required>
						</div>
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Middle Name:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="middlename" name="middlename" required>
						</div>
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Department ID:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="department_id" name="department_id" required>
						</div>
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Contact Number:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="contact_number" name="contact_number" required>
						</div>
						<div class="form-group">
							<input type="checkbox" name="status" data-toggle="toggle"
							 @if(!empty($showData)) @if($showData->status == 1) checked="true" @endif
							  @else checked="true" @endif>
							<label>&nbsp;: Status</label>
						</div>
						{!! csrf_field() !!}
						<input type="hidden" name="id" value="" />
						<input name="submit" type="submit" class="btn btn-success pull-right" value="Save">
					</div>
				</div>
			</form>
			<div class="col-md-6">
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of User Office Information
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Office ID Number</th>
										<th class="text-center">First Name</th>
										<th class="text-center">Last Name</th>
										<th class="text-center">Middle Name</th>
										<th class="text-center">Department ID</th>
										<th class="text-center">Contact Number</th>
										<th class="text-center">Status</th>
										<th class="text-center">Option</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($useroffice_list as $item)
									<tr> 
										<td class="text-center">{{$item->id}}</td>
										<td class="text-center">{{$item->office_id_number}}</td>
										<td class="text-center">{{$item->firstname}}</td>
										<td class="text-center">{{$item->lastname}}</td>
										<td class="text-center">{{$item->middlename}}</td>
										<td class="text-center">{{$item->department_id}}</td>
										<td class="text-center">{{$item->contact_number}}</td>
										<td class="text-center">{{$item->status}}</td>
										<td class="text-center">
											<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal{{$item->id}}">Edit</button>
											<!-- Modal -->
											<div id="myModal{{$item->id}}" class="modal fade" role="dialog">
											  	<div class="modal-dialog">

											    	<!-- Modal content-->
												    <div class="modal-content">
													    <form method="post" action="{{action('UserController@updateuserofficeinfo')}}">
													      	<div class="modal-header">
													        	<button type="button" class="close" data-dismiss="modal">
													        		&times;</button>
													        	<h4 class="modal-title">Update User Office Information</h4>
													      	</div>
													      	<div class="modal-body">
													        	
																<div class="col-md-12" style="color:#000;">
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Office ID Number:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="office_id_number" 
												 						name="office_id_number" value="{{$item->office_id_number}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">First Name:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="firstname" 
												 						name="firstname" value="{{$item->firstname}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Last Name:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="lastname" 
												 						name="lastname" value="{{$item->lastname}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Middle name:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="middlename" 
												 						name="middlename" value="{{$item->middlename}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Department ID:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="department_id" 
												 						name="department_id" value="{{$item->department_id}}" required>
																	</div>
																	<div class="form-group">
												  						<label for="usr" style="color:#000;">Contact Number:</label>
												 						<input type="text" style="color:#000;" class="form-control" id="contact_number" 
												 						name="contact_number" value="{{$item->contact_number}}" required>
																	</div>
																	<div class="form-group">
																		<input type="checkbox" name="status" data-toggle="toggle"
																		 @if(!empty($showData)) @if($showData->status == 1) checked="true" @endif
																		  @else checked="true" @endif>
																		<label>&nbsp;: Status</label>
																	</div>
																	<input type="hidden" name="id" value="{{$item->id}}">
																	{!! csrf_field() !!}
																</div>
														    </div>
													      	<div class="modal-footer">
													        	<button type="submit" class="btn btn-success pull-right">
													        	<i class="fa fa-floppy-o" aria-hidden="true">
													        	</i>&nbsp;&nbsp;Save</button>
													      	</div>
													    </form>
												    </div>

											  	</div>
											</div>
												<input name="_method" type="hidden" value="DELETE">
            									<a href="{{url('delete/userofficeinfo/'. $item->id)}}">Delete</a>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop