@extends('layouts.admin-default')
@section('content')

<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Dashboard</h3>
			</div>
		</div>
		<div class="container-row">
			<div class="row">
			  	<div class="col-sm-6 col-md-6">
			    	<div class="thumbnail">
			    		
			      		<div class="caption">
					        <p><a href="{{url('/admin/transfer/in/new')}}" class="btn btn-primary" role="button"> <i class="fa fa-plus-circle"></i> New Transfer In</a>
					        <a href="{{url('/bulk/upload/in')}}" class="btn btn-info">Bulk Transaction</a> </p>
			      		</div>
			      		<table id="transIn">
				    		<thead>
				    			<tr>
				    				<th>Doc ID</th>
				    				<th>Site</th>
				    				<th>Publish Date</th>
				    				<th>Status</th>
				    				<th>Action</th>
				    			</tr>
				    		</thead>
				    		<tbody>

				    			@foreach($transIn as $row)
				    			<tr>
				    				<td>{{$row->id}}</td>
				    				<!-- get site -->
				    				<?php
				    					$item = App\Http\Model\PlatformProductListHistoryModel::where('document_id',$row->id)->where('type',1)->select('PPL_id')->first();
										if($item){
											$plat_item = App\Http\Model\PlatformProductListModel::where('platform_product_list_id',$item->PPL_id)->select('platform_id')->first();
											$platform_name = App\Http\Model\PlatformModel::where('id',$plat_item->platform_id)->select('platform_name')->first();
											$p_name = $platform_name->platform_name;
										} else{
											$p_name = "No Site";
										}
									?>
				    				<!-- end get site -->
				    				<td>{{$p_name}}</td>
				    				<td>{{$row->selectedDate}}</td>
				    				<td>@if($row->status==1)
				    						Posted
				    					@elseif($row->status==0)
				    						Save
				    					@else
				    						New
			    						@endif		
				    				</td>
				    				<td><a href="{{url('/admin/transfer/in/' . $row->id)}}">View</a></td>
				    			</tr>
				    			@endforeach
				    		</tbody>
				    	</table>
			    	</div>
			  	</div>
			  	<div class="col-sm-6 col-md-6">
			    	<div class="thumbnail">
			      		<div class="caption">
					        <p><a href="{{url('/admin/transfer/out/new')}}" class="btn btn-primary" role="button">  <i class="fa fa-plus-circle"></i> New Transfer Out</a> </p>
			      		</div>
			      		<table id="transOut">
				    		<thead>
				    			<tr>
				    				<th>Doc ID</th>
				    				<th>Site</th>
				    				<th>Status</th>
				    				<th>Publish Date</th>
				    				<th>Action</th>
				    			</tr>
				    		</thead>
				    		<tbody>
				    			@foreach($transOut as $row)
				    			<tr>
				    				<td>{{$row->id}}</td>
				    				<!-- get site -->
				    				<?php
				    					$item = App\Http\Model\PlatformProductListHistoryModel::where('document_id',$row->id)->where('type',0)->select('PPL_id')->first();
				    					if($item){
					    					$plat_item = App\Http\Model\PlatformProductListModel::where('platform_product_list_id',$item->PPL_id)->select('platform_id')->first();
					    					$platform_name = App\Http\Model\PlatformModel::where('id',$plat_item->platform_id)->select('platform_name')->first();

					    					$p_name = $platform_name->platform_name;
				    					} else{
				    						$p_name = "No Site";
				    					}
				    				?>
				    				<!-- end get site -->
				    				<td>  {{$p_name}}</td>
				    				<td>@if($row->status==1)
				    						Posted
				    					@elseif($row->status==0)
				    						Save
				    					@else
				    						New
			    						@endif		
				    				</td>
				    				<td>{{$row->created_at->format('d-m-Y')}}</td>
				    				<td><a href="{{url('/admin/transfer/out/' . $row->id)}}">View</a></td>
				    			</tr>
				    			@endforeach
				    		</tbody>
				    	</table>
			    	</div>
			  	</div>
			  	<hr class="col-md-12" />
			  	<!-- <div class="col-sm-6 col-md-6">
			    	<div class="thumbnail">
			    		
			      		<div class="caption">
					        <h3>Sales Analysis Report</h3>
					        <hr />
					        <div id="salesReport" style="width:96%;margin:0 auto;"></div>
					        $ l a v a->render("AreaChart","Sales","salesReport");
					        <p> 
					        	<a href="{{url('/reports/sales/analysis')}}" class="btn btn-primary" role="button"><i class="fa fa-table"></i> View More</a>
					        </p>
			      		</div>
			    	</div>
			  	</div> -->

			  	<div class="col-sm-6 col-md-4">
			    	<div class="thumbnail">
			    		<h4>Top 5 Site / Branch Item Count</h4>
			    		<small>Highest remaining stock on inventory</small>
			    		<div class="caption">
			    			<table id="itemCount">
					    		<thead>
					    			<tr>
					    				<th>Site</th>
					    				<th>Quantity</th>
					    			</tr>
					    		</thead>
					    		<tbody>
					    			@foreach($itemCounter as $row)
					    				<tr>
					    					<?php 
					    						$plat_name = App\Http\Model\PlatformModel::where('id',$row->platform_id)->select('platform_name')->first();
					    					?>
					    					<td>{{$plat_name['platform_name']}}</td>
					    					<td>{{$row->totalQty}}</td>
					    				</tr>
					    			@endforeach
					    		</tbody>
				    		</table>
			    		</div>
			    	</div>
			    </div>
			  	<div class="col-sm-6 col-md-8">
			    	<div class="thumbnail">
			    		<h4>Reports</h4>
			    		<div class="caption">
					        <p>
					        	<a href="{{url('/reports/search/item')}}" class="btn btn-primary" role="button"><i class="fa fa-search"></i> Search Items</a> 
					        	<a href="{{url('/reports/stock/evaluation')}}" class="btn btn-primary" role="button"><i class="fa fa-indent"></i> Stock Evaluation</a>
					        	<a href="{{url('/reports/sales/analysis')}}" class="btn btn-primary" role="button"><i class="fa fa-database"></i> Sales Analysis</a> 
								@if(Session::get('isLogin') == 16)
					        		<a href="{{url('/reports/adjust/product')}}" class="btn btn-primary" role="button"><i class="fa fa-beer"></i> Adjust Product</a> 
								@endif
					    	</p>

			      		</div>
			    	</div>
			  	</div>
			</div>
		</div>
	</section>
</section>
<script type="text/javascript">
	$(document).ready( function () {
		$('#itemCount').DataTable({
			dom:'',
			"order": [[1, "desc" ]],
		});
    	$('#transIn').DataTable({
    		"order": [[ 0, "desc" ]],
    		"aoColumns": [ {"bSearchable": true}, {"bSearchable": true}, {"bSearchable": false}, {"bSearchable": false},{"bSearchable": false}]
    	});
    	$('#transOut').DataTable({
    		"order": [[ 0, "desc" ]],
    		"aoColumns": [ {"bSearchable": true},  {"bSearchable": true}, {"bSearchable": false}, {"bSearchable": false},{"bSearchable": false}]
    	});
	});
</script>
@stop