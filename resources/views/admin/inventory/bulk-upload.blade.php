@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Reports : Stock Evaluation</h3>
			</div>
		</div>
		<div class="container-row">
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}
			</div>
			@endif
			<form enctype="multipart/form-data" method="post" action="{{action('InventoryController@postBulkUploadIn')}}">
				<div class="col-md-6">
					<div class="form-group ">
					 	<label for="platform" class=" col-form-label">Platform / Branch Name: </label>
					 	<input type="text" id="platform" name="platform" class="form-control" placeholder="Search branch or platform name" required="true" />
					 	<small><strong>NOTE : </strong>Upload Transfer In List should consist 300 lines or less</small>
					 	<p>Get CSV file Format : <a href="{{asset('assets-admin/misc/inmportTransin.csv')}}">download</a></p>
					</div>
				</div>
				<div class="col-md-6" id="bulk-upload-div">
					<div class="panel panel-info">
						<div class="panel-heading">
							Bulk Product Upload
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="file">select a file to upload</label>
							<input type="file" name="file" id="file" class="form-control">
							{!! csrf_field() !!}
						</div>
					</div>
					<div class="panel-footer">
						<div class="form-group">
							<input type="submit" class="btn btn-primary pull-right" value="Start Upload">
						</div>
					</div>
				</div>
			</form>
			<div class="col-md-12">
				<hr />
				Information
			</div>
		</div>
	</section>
</section>
<script type="text/javascript">
	$(function() {
		var skupath = "{{url('/branch/search/autocomplete')}}";
	  	$('#platform').typeahead({
	  		source:function(query,process){
	  			return $.get(skupath, {query:query},function(data){
	  				return process(data);
	  			});
	  		}
	  	});
	});
</script>
@endsection