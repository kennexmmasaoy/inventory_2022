@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Zip Upload</h3>
			</div>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div class="clear"></div>
			<form method="post">
				<div class="col-md-6 well" style="color:#000;">
						<div class="form-group">
	  						<label for="usr" style="color:#000;">Product Name:</label>
	 						<input type="text" style="color:#000;" class="form-control" id="goods_name" name="goods_name" required @if(!empty($ep)) value="{{$editProduct->goods_name}}" @endif>
						</div>
					
						<input type="hidden" name="id" value="" />
						<input name="submit" type="submit" class="btn btn-success pull-right" value="Save">
					</div>
				</div>
			</form>
			<div class="col-md-6">
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of Zip Upload
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center">Materials</th>
										<!-- <th class="text-center">created at</th>
										<th class="text-center">Updated at</th> -->
										<th class="text-center">Options</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									
									<tr> 
										<td class="text-center"></td>
										<td class="text-center"></td>
										<!-- <td class="text-center"></td>
										<td class="text-center"></td> -->
										<td class="text-center">
											<div class="btn-group-vertical btn-group-xs">
												<a href="" type="button" class="btn btn-info">
													<div class="col-md-1">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
													</div>
													Edit
												</a>
												
												<a type="button" class="btn btn-danger" href="">
													<div class="col-md-1">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</div>
													Delete
												</a>
											</div>
										</td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		
	</section>
</section>
@stop