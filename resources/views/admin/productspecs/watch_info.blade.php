@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Watch Information</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			
			<div class="col-md-8">
				<div class="btn-group">
					<a href="{{url('/productspecs/add_watchinfo')}}" type="button" class="btn btn-primary">
						<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add
					</a>
				</div>
				<div class="btn-group">
					<a href="#" type="button" class="btn btn-info" id="bulk_upload">
						<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Bulk Upload
					</a>
				</div>
			</div>
			<div class="col-xs-4">
				{!! Form::open(['method'=>'GET','url'=>'/productspecs/watch_info','role'=>'search'])  !!}
			      <div class="input-group custom-search-form">
			          <input type="text" class="form-control" name="search" placeholder="Search...">
			          <span class="input-group-btn">
			              <button class="btn btn-default-sm" type="submit">
			                  <i class="fa fa-search"></i>
			              </button>
			          </span>
			      </div>
			      {!! Form::close() !!}
			</div>
			<div class="clear"></div>
			<div class="col-md-12" id="bulk-upload-div" hidden>
				<form enctype="multipart/form-data" method="post" action="{{action('ProductInfoController@uploadBulk')}}">
					<div class="panel panel-info">
						<div class="panel-heading">
							Bulk Product Upload
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="file">select a file to upload</label>
							<input type="file" name="file" id="file" class="form-control">
							{!! csrf_field() !!}
						</div>
					</div>
					<div class="panel-footer">
						<div class="form-group">
							<input type="submit" value="Upload">
					</div>
				</div>
				</form>
			</div>
			<div class="clear"></div>
			<div class="col-md-12"><hr>
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of Watch Information
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">Image(Yes / No)</th>
										<th class="text-center">SKU</th>
										<th class="text-center">Product Name</th>
										<th class="text-center">Brand</th>
										<th class="text-center">Product Description</th>
										
										<th class="text-center">Price</th>
										<th class="text-center">Options</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									@foreach($info_list as $item)
									<tr> 
										<td class="text-center">
											@if($item->image_id)
											@else
												No
											@endif
										</td>
										<td class="text-center">{{$item->sku}}</td>
										<td class="text-center">{{$item->product_name}}</td>
										<td class="text-center">{{$item->brand_name}}</td>
										<td class="text-center">{{$item->product_description}}</td>
										
										<td class="text-center">{{$item->price}}</td>
										<td class="text-center">
											<a href="{{url('/productspecs/edit_watchinfo/'.$item->info_id)}}" type="button" class="btn btn-info btn-xs">Edit</button>
											<input name="_method" type="hidden" value="DELETE">
        									<a href="{{url('delete/watchinfo/'. $item->info_id)}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								{{$info_list->render()}}
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
<script type="text/javascript">
	$("#bulk_upload").click(function(e){
		$("#bulk-upload-div").removeAttr("hidden");
	});
</script>
@stop