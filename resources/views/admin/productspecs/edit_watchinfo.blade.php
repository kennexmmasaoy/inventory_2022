@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Watch Information</h3>
			</div>
			@if(Session::has('success'))
			<div class="alert alert-dismissible alert-success">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <strong>Well done!</strong> {{Session::get('success')}}.
			</div>
			@endif
			<form method="post" action="{{action('ProductinfoController@updateWatchInfo')}}" enctype="multipart/form-data">
				<div class="col-md-12 well">
						<div class="form-group col-xs-6">
							@if(!empty($item->image_id))
							<?php  $image = App\Http\Model\ImageModel::where('id',$item->image_id)->select('savename')->first();  ?>
							<img src="{{asset('/upload/'. $image['savename'])}}" style="height:100px;width:100px">
							@else 
							<img class="img-responsive menu-thumbnails" src="https://d30y9cdsu7xlg0.cloudfront.net/png/152175-200.png" />
							@endif

							@if(!empty($getWatchImage)) <input type="hidden" name="image" value="{{$getWatchImage->image_id}}" /> @endif
							<input type="file" name="image" id="image" class="btn btn-danger">
							<small><strong>Image size required is 360px by 360px and maximum 2MB 72 dpi file size</strong></small>
						</div>
						<div class="form-group col-xs-3">
	  						<label for="">Upload ZipFile For Lazada</label>
	  						@if(!empty($item->id))
							<?php  $zip = App\Http\Model\ZipModel::where('watch_info_id',$item->id)->where('channel','=',0)->first();  ?>
							<a href="{{asset('/upload/zip'. $zip['url'])}}" download>download</a>
							@else 
							@endif
							<span>{{$zip->url}}</span>
							<input type="file" name="zip" id="zip" class="btn btn-danger">
						</div>
						<div class="form-group col-xs-3">
	  						<label for="">Upload ZipFile For Shopee</label>
	  						@if(!empty($item->id))
							<?php  $zip = App\Http\Model\ZipModel::where('watch_info_id',$item->id)->where('channel','=',1)->first();  ?>
							<a href="{{asset('/upload/zip'. $zip['url'])}}" download>download</a>
							@else 
							@endif
							<span>{{$zip->url}}</span>
	 						<input type="file" name="zip1" id="zip1" class="btn btn-danger">
						</div>
						<div class="form-group col-xs-3">
	  						<label for="">Upload ZipFile For Attigo</label>
	  						@if(!empty($item->id))
							<?php  $zip = App\Http\Model\ZipModel::where('watch_info_id',$item->id)->where('channel','=',2)->first();  ?>
							<a href="{{asset('/upload/zip'. $zip['url'])}}" download>download</a>
							@else 
							@endif
							<span>{{$zip->url}}</span>
	 						<input type="file" name="zip2" id="zip2" class="btn btn-danger">
						</div>
						<div class="form-group col-xs-3">
	  						<label for="">Upload ZipFile For UniSilverTime</label>
	  						@if(!empty($item->id))
							<?php  $zip = App\Http\Model\ZipModel::where('watch_info_id',$item->id)->where('channel','=',3)->first();  ?>
							<a href="{{asset('/upload/zip'. $zip['url'])}}" download>download</a>
							@else 
							@endif
							<span>{{$zip->url}}</span>
	 						<input type="file" name="zip3" id="zip3" class="btn btn-danger">
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">SKU</label>
	 						<input type="text" class="form-control" id="sku" name="sku" value="{{$item->sku}}" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Product Name:</label>
	 						<input type="text" class="form-control" id="product_name" name="product_name" value="{{$item->product_name}}" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Description1</label>
	 						<textarea class="form-control" type="text" id="product_description" name="product_description" rows="3">{{$item->product_description}}</textarea>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Description2</label>
	 						<textarea class="form-control" type="text" id="product_description_2" name="product_description_2" rows="3">{{$item->product_description_2}}</textarea>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Color:</label>
	 						<select class="form-control" name="color" id="color">
					 			<option>Select Color</option>
					 			@foreach($color_name as $row)
							    <option value="{{$row->id}}"
							    	@if($item->color == $row->id) selected @endif>{{$row->color_name}},{{$row->color_hex}},{{$row->color_rgb}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Brand:</label>
	  						<select class="form-control" name="brand" id="brand">
					 			<option>Select Brand</option>
					 			@foreach($brand as $row)
							    <option value="{{$row->id}}" @if($item->brand == $row->id) selected @endif>{{$row->brand_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Dial:</label>
	  						<select class="form-control" name="dial" id="dial">
					 			<option>Select Color</option>
					 			@foreach($dial as $row)
							    <option value="{{$row->id}}" @if($item->dial == $row->id) selected @endif>{{$row->dial_name}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Case Material:</label>
	 						<select class="form-control" name="case_material" id="case_material">
					 			<option>Select Color</option>
					 			@foreach($case_material as $row)
							    <option value="{{$row->id}}" @if($item->case_material == $row->id) selected @endif>{{$row->material}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Face Height with Bezel:</label>
	  						<select class="form-control" name="face_height_with_bezel" id="face_height_with_bezel">
					 			<option>Select Color</option>
					 			@foreach($face_height as $row)
							    <option value="{{$row->id}}"
							    	@if($item->face_height_with_bezel == $row->id) selected @endif>{{$row->size}}{{$row->length_indicator}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Case Width:</label>
	  						<select class="form-control" name="case_width" id="case_width">
					 			<option>Select Color</option>
					 			@foreach($case_width as $row)
							    <option value="{{$row->id}}"
							    	@if($item->case_width == $row->id) selected @endif>{{$row->size}}{{$row->length_indicator}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Case Thickness:</label>
	  						<select class="form-control" name="case_thickness" id="case_thickness">
					 			<option>Select Color</option>
					 			@foreach($case_thickness as $row)
							    <option value="{{$row->id}}"
							    	@if($item->case_thickness == $row->id) selected @endif>{{$row->size}}{{$row->length_indicator}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Max Wrist Circumference:</label>
	  						<select class="form-control" name="max_wrist_circumference" id="max_wrist_circumference">
					 			<option>Select Color</option>
					 			@foreach($max_wrist_circumference as $row)
							    <option value="{{$row->id}}"
							    	@if($item->max_wrist_circumference == $row->id) selected @endif>{{$row->size}}{{$row->length_indicator}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="usr">Clasp Type:</label>
	  						<select class="form-control" name="clasp_type" id="clasp_type">
					 			<option>Select Color</option>
					 			@foreach($clasp_type as $row)
							    <option value="{{$row->id}}" @if($item->clasp_type == $row->id) selected @endif>{{$row->type}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="">Water Resistance:</label>
	  						<select class="form-control" name="water_resistance" id="water_resistance">
					 			<option>Select Color</option>
					 			@foreach($water_resistance as $row)
							    <option value="{{$row->id}}" @if($item->water_resistance == $row->id) selected @endif>{{$row->depht}}</option>
							    @endforeach
					  		</select>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="fatures">Other Features:</label>
	 						<input type="text" class="form-control" id="other_features" name="other_features" value="{{$item->other_features}}" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="warranty">Warranty:</label>
	 						<input type="text" class="form-control" id="warranty" name="warranty" value="{{$item->warranty}}" required>
						</div>
						<div class="form-group col-xs-6">
	  						<label for="price" style="color:#000;">Price:</label>
	 						<input type="number" class="form-control" id="price" name="price" value="{{$item->price}}" required>
						</div>
						{!! csrf_field() !!}
						<input type="hidden" name="id" value="{{$item->id}}">
						<input type="submit" class="btn btn-success" value="Save">
						<a href="{{url('/productspecs/watch_info')}}" type="button" class="btn btn-danger">
							<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
					</div>
			</form>			
		</div>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</section>
</section>
@stop