@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Product Management</h3>
			</div>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Library</a></li>
				<li class="breadcrumb-item active">Data</li>
			</ol>
			<div class="col-md-6">
				<div class="btn-group">
					<a href="{{url('/add/product')}}" type="button" class="btn btn-info">
						<i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add
					</a>
				</div>
			</div>
			<div class="input-group pull-right">
				
				 
				<div class="input-group custom-search-form">
                        <div class="input-group-btn">
                        	<input type="text" class="input_search form-control" name="search" placeholder="Search..." style="height: 38px;">
                            <div class="btn-group">
                                <select name="search_type" class="dropdown">
									<option value="name">Name</option>
							        <option value="sku">SKU</option>
							        <option value="status">Status (Enabled / Disable)</option>
							    </select>
                            </div>
                        </div>
                        <span class="input-group-btn">
					        <button class="btn btn-default-sm" type="submit" style="min-height: 40px;"> 
					            <i class="fa fa-search"></i>
					        </button>
					    </span>
				</div>
				<div class="clear"></div><br><br>
			</div>

			<div class="col-md-12">
				<div class="panel panel-danger">
					<div class="panel-heading">&nbsp;
						<h3 class="panel-title">	
							List of Registered Products
						</h3>
					</div>
					<div class="panel-body">
						<div class="table-responsive">          
							<table class="table table-bordered table-condensed">
								<thead style="color:#000;">
									<tr>
										<th class="text-center">ID</th>
										<th class="text-center" style="width:25%;">Image</th>
										<th class="text-center">Product Name</th>
										<th class="text-center">Selling Price</th>
										<th class="text-center">Original Price</th>
										<th class="text-center">Discount</th>
										<th class="text-center">Stocks</th>
										<th class="text-center">Status</th>
										<th class="text-center">Options</th>
									</tr>
								</thead>
								<tbody style="color:#000;">
									
									<tr> 
										<td></td>
										<td class="text-center">
											<div class="container-fluid">
												 
												<img src="" style="height:30%;width:30%;">
											</div>
										</td>
										<td class="text-center"></td>
										<td class="text-center"></td>
										<td class="text-center"></td>
										<td class="text-center"></td>
										<td class="text-center"></td>
										<td class="text-center"><strong>Disable<strong>Enable</td>
										<td class="text-center">
											<div class="btn-group-vertical btn-group-xs">
												<a href="" type="button" class="btn btn-info">
													<div class="col-md-1">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
													</div>
													Edit
												</a>
												
												<a type="button" class="btn btn-success" href="">
													<i class="fa fa-check-square" aria-hidden="true" ></i>&nbsp;Enable
												</a>
												
												<a type="button" class="btn btn-warning"  href="">
													<i class="fa fa-window-close" aria-hidden="true"></i>&nbsp;Disable
												</a>
												
												<a type="button" class="btn btn-danger" href="">
													<div class="col-md-1">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</div>
													Delete
												</a>
											</div>
										</td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer">
						<nav aria-label="Page navigation">
							<ul class="pagination pagination-sm">
								
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
		
	</section>
</section>
<script>
	$('select[name="search_type"]').on('change',function(){
	   if($(this).val()=="status")
	   {
	    	$('.input_search').hide();
	        $('.btn_dropdown').show();
	   }
	   else
	   {
	        $('.input_search').show();
	        $('.btn_dropdown').hide();
	   }
	});
</script>
<style>
	.btn_dropdown{
	   display:none;
	}
</style>
@stop