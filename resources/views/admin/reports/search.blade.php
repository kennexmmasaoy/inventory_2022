@extends('layouts.admin-default')
@section('content')
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Reports : Item Search</h3>
			</div>
		</div>
		<div class="container-row">
			{!! Form::open(['method'=>'GET','url'=>'reports/search/item','role'=>'search'])  !!}		
				<div id="date-range">

					<div class="col-md-2">
						<div class="form-group ">
						 	<label for="staticEmail" class="col-form-label">Start Date : </label>
						 	<input type="date" id="start_date" name="start_date" @if($search_info['startDate']) value="{{ $search_info['startDate'] }}" @else value="{{ date('Y-m-01')}}" @endif  class="form-control" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group ">
						 	<label for="staticEmail" class="col-form-label">End Date :</label>
						 	<input type="date" id="end_date" name="end_date"  class="form-control" @if($search_info['endDate']) value="{{ $search_info['endDate'] }}" @else value="{{ date('Y-m-d') }}" @endif />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group ">
					 	<label for="staticEmail" class="col-form-label">SKU / Barcode : </label>
					 	<input type="text" id="barcode" name="barcode" class="form-control" value="{{$search_info['barcode']}}"/>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group ">
					 	<label for="staticEmail" class=" col-form-label">Report Type : </label>
					 	<select class="form-control" id="reportType" name="reportType">
							<option value="">Please Select Type</option>
							<option value="1" @if($search_info['reportType'] == 1) selected="true" @endif>Stock Card</option>
							<option value="2" @if($search_info['reportType'] == 2) selected="true" @endif>Consolidated</option>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label for="searchItem" class=" col-form-label">Action</label>
						<input type="submit" class="btn btn-warning" id="submit-button" value="Search" />
					</div>
				</div>
			{!! Form::close() !!}
			@if($search_info['reportType']!=2)
			<div class="container-row" id="stock-card">
				<table id="stock-card-tbl">
					<thead>
						<tr>
							<th>Document Date</th>
							<th>Type</th>
							<th>Document Number</th>
							<th>IN</th>
							<th>Out</th>
							<th>Site</th>
						</tr>
					</thead>
					<tbody>
						@foreach($reportStockCard as $key => $item)

							@if($reportStockCard)
							<tr>
								
								<td>{{$item->created_at}}</td>
								<td>@if($item->transType==1) In @elseif($item->transType=='2') Adjustment @else Out @endif</td>
								<td>{{$item->document_id}}</td>
								<td>@if($item->transType==1){{$item->quantity}}@else 0 @endif</td>
								<td>@if($item->transType==0  || $item->transType==2 ){{$item->quantity}}@else 0 @endif</td>
								
								<td>{{$item->platform_name}}</td>
							</tr>
							@else
							<tr>
								<td colspan="7">No Data to be displayed</td>
							</tr>
							@endif
						@endforeach
					</tbody>
					<tfoot>
			            <tr>
			            	<th></th>
			            	<th></th>
			                <th></th>
			                <th style="text-align:right">Total Balance:</th>
			                <th></th>
			                <th></th>
			            </tr>
		        </tfoot>
				</table>
			</div>
			@else
			<div class="container-row" id="consolidated">
				<table id="consolidated-tbl">
					<thead>
						<tr>
							<th> Site</th>
							<th> Quantity </th>
						</tr>
					</thead>
					<tbody>
						@foreach($reportStockCard as $item)
							@if($reportStockCard)
							<tr>
								<td>{{$item->platform_name}}</td>
								<td>{{$item->finalQuantity}}</td>
							</tr>
							@else
							<tr class="text-center">
								<td colspan="2">No Data to be displayed</td>
							</tr>
							@endif
						@endforeach
						
					</tbody>
				</table>
			</div>
			@endif
			<div class="container-row" id="consolidated" hidden>
				<table id="consolidated-tbl">
					<thead>
						<tr>
							<th> Site </th>
							<th> Quantity </th>
						</tr>
					</thead>
					<tbody>
						<tr class="text-center">
							<td colspan="2">No Data to be displayed</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="container-row" id="stock-card" hidden>
				<table id="consolidated-tbl">
					<thead>
						<tr>
							<th> Site </th>
							<th> Quantity </th>
						</tr>
					</thead>
					<tbody>
						<tr class="text-center">
							<td colspan="2">No Data to be displayed</td>
						</tr>
					</tbody>

				</table>
			</div>
		</div>
	</section>
</section>
<div id="snackbar">
	<p id="msg_response" class="msg_response"></p>
</div>
<script type="text/javascript">
	$(document).ready( function () {	
		var numFormat = $.fn.dataTable.render.number( '\,', '.', 0, '&#8369;' ).display;
		var numFormatQty = $.fn.dataTable.render.number( '\,', '.', 0 ).display;
		$('#stock-card-tbl').DataTable({
			"order": [[0,"desc"]],
    		dom: 'Blrtip',
    		buttons: [
	            {
	                extend: 'print',
	                footer: true,
	                title: '',
	                messageTop: function () {
	                        return '@if($search_info['reportType'] == 1) Stock Card :  @else Consolidated :  @endif {{$search_info['barcode']}}';
	                },
	                messageBottom: null,
	                 customize: function ( win ) {
	                  $(win.document.body).find( 'table' )
	                        .addClass( 'compact' )
	                        .css( 'font-size', 'inherit' );
	                 }
	            }
	        ], 
	        "sPaginationType": "full_numbers",
            "columnDefs": [
                { "searchable": false, "targets": [0, 1] }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 			
	 			// Total over all pages
	            outProd = api
	                .column( 4 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            // Total over all pages
	            inProd = api
	                .column( 3 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 4, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 4 ).footer() ).html(
	               intVal(numFormatQty(inProd)) - intVal(numFormatQty(outProd)) +' pcs'
	            );
	        }
    	});
    	$("#consolidated-tbl").DataTable();
	});
	$("#reportType").change(function(e){
		var reportTypeVal = $("#reportType").val();
		if(reportTypeVal == 2){
			$("#stock-card").prop('hidden','true');
			$("#platform-select").prop('hidden','true');
			$("#date-range").prop('hidden','true');
			$("#consolidated").removeAttr('hidden');
		}else{
			$("#consolidated").prop('hidden','true');
			$("#stock-card").removeAttr('hidden');
			$("#platform-select").removeAttr('hidden');
			$("#date-range").removeAttr('hidden');
		}
	});
	$("#submit-button").click(function(e){
		var barcode = $("#barcode").val();
		var endDate = $("#end_date").val();
		var startDate = $("#start_date").val();
		var reportType = $("#reportType").val();
		if(barcode == ""){
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		    $("#msg_response").html("Barcode / Sku cannot be empty");
		    return false;
		}
		if(reportType == ""){
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		    $("#msg_response").html("Please select report type");
		    return false;
		}
		 	return true;
	});
	$(function() {
		var skupath = "{{url('/sku/search/autocomplete')}}";
	  	$('#barcode').typeahead({
	  		source:function(query,process){
	  			return $.get(skupath, {query:query},function(data){
	  				return process(data);
	  			});
	  		}
	  	});
	});
</script>
@stop