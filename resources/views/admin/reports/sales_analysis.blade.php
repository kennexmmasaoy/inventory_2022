@extends('layouts.admin-default')
@section('content')
<div id="snackbar">
			<p id="msg_response" class="msg_response"></p>
		</div>
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Reports : Sales Analysis</h3>
			</div>
		</div>
		<div class="container-row">
			{!! Form::open(['method'=>'GET','url'=>'reports/sales/analysis','role'=>'search'])  !!}		
				<div id="date-range">
					<div class="col-md-2">
						<div class="form-group ">
						 	<label for="start_date" class="col-form-label">Start Date : </label>
						 	<input type="date" id="start_date" name="start_date" @if($stock_eval_info['start_date']) value="{{ $stock_eval_info['start_date'] }}" @else value="{{ date('Y-m-01') }}" @endif class="form-control" />
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group ">
						 	<label for="end_date" class="col-form-label">End Date :</label>
						 	<input type="date" id="end_date" name="end_date"  class="form-control"  @if($stock_eval_info['endDate']) value="{{ $stock_eval_info['endDate'] }}" @else value="{{ date('Y-m-d') }}" @endif />
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group ">
					 	<label for="barcode" class="col-form-label">SKU / Barcode : </label>
					 	<input type="text" id="barcode" name="barcode" class="form-control" placeholder="SKU or Barcode" value="{{$stock_eval_info['barcode']}}"/>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group ">
					 	<label for="platform" class=" col-form-label">Platform / Branch Name: </label>
					 	<input type="text" id="platform" name="platform" class="form-control" placeholder="Search branch or platform name" value="{{$stock_eval_info['platform']}}"/>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label for="searchItem">Action</label>
						<input type="submit" class="btn btn-warning" id="submit-button" value="Generate" />
					</div>
				</div>
			{!! Form::close() !!}
		</div>
		<div class="container-row">
			<table id="stock-evalulation-report">
				<thead>
					<tr>
						<th>Site</th>
						<th>Date</th>
						<th>Barcode</th>
						<th>Code</th>
						<th>Description</th>
						<th>On Hand</th>
						<th>Price</th>
						<th>Total Price</th>
					</tr>
				</thead>
				<tbody>
					@if($stock_eval_info['platform'] || $stock_eval_info['barcode'] || $stock_eval_info['endDate'] || $stock_eval_info['start_date'])
					
						@foreach($info as $item)
						<tr>
							<?php 
								$plat_info = App\Http\Model\PlatformModel::find($item->platform_id);
							?>
							<td>{{$plat_info->platform_name}}</td>
							<td>{{$item->created_at->format('d-m-Y')}}</td>
							<td>{{$item->barcode}}</td>
							<td>{{$item->sku}}</td>
							<td>{{ str_limit($item->product_description,30) }}</td>
							<td>{{$item->quantity}}</td>
							<td>{{number_format($item->price,2)}}</td>
							<td>{{number_format($item->quantity * $item->price,2)}}</td>
						</tr>
						@endforeach
					@endif
				</tbody>
				<tfoot>
		            <tr>
		            	<th></th>
		            	<th></th>
		            	<th></th>
		                <th></th>
		                <th style="text-align:right">Total:</th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
			</table>
		</div>
	</section>
</section>


		
<script type="text/javascript">
	$(document).ready( function () {
		var numFormat = $.fn.dataTable.render.number( '\,', '.', 0, '&#8369;' ).display;
		var numFormatQty = $.fn.dataTable.render.number( '\,', '.', 0 ).display;

    	$('#stock-evalulation-report').DataTable({
    		"order": [[ 1, "desc" ]],
    		dom: 'lBrtip',
    		buttons: [
		        { 	extend: 'print', 
		        	footer: true,
		        	title: '',
		        	messageTop: function () {
	                        return '@if($stock_eval_info["platform"]) {{$stock_eval_info["platform"]}}  @else All Branches @endif with @if(!$stock_eval_info["barcode"]) All Products @else {{$stock_eval_info["barcode"]}} @endif';
	                },
					customize: function ( win ) {
	                    $(win.document.body)
	                        .css( 'font-size', '10pt' )
	 
	                    $(win.document.body).find( 'table' )
	                        .addClass( 'compact' )
	                        .css( 'font-size', 'inherit' );
	                }
		        },
		        { 	extend: 'copy',  
		        	footer: true,
	            },
	            { 	extend: 'excel',
		        	footer: true,
		        	title: 'Sales Analysis  {{ date("Y-m-d") }}',
		        	messageTop: function(){
		        		return '@if($stock_eval_info["platform"]) {{$stock_eval_info["platform"]}}  @else All Branches @endif with @if(!$stock_eval_info["barcode"]) All Products @else {{$stock_eval_info["barcode"]}} @endif';
		        	}
	            },
	            { 	extend: 'csv',
		        	footer: true,
	            },
	            { extend: 'pdf',
		        	footer: true,
		        	title: 'Sales Analysis  {{ date("Y-m-d") }}',
		        	messageTop: function(){
		        		return '@if($stock_eval_info["platform"]) {{$stock_eval_info["platform"]}}  @else All Branches @endif with @if(!$stock_eval_info["barcode"]) All Products @else {{$stock_eval_info["barcode"]}} @endif';
		        	}
	            }

	        ],
	        "footerCallback": function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 			
	 			// Total over all pages
	            total = api
	                .column( 5 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 5, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 5 ).footer() ).html(
	                numFormatQty(total) +' pcs'
	            );

	            // Total over all pages
	            total = api
	                .column( 6 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 6, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 6 ).footer() ).html(
	                 numFormat(total) 
	            );

	            // Total over all pages
	            total = api
	                .column( 7 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 7, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 7).footer() ).html(
	               numFormat(total)
	            );
	        }
    	});
    });
$(function() {
  	$("#platform").blur(function(e){
		checkPlatform();
  	});
  	$("#barcode").blur(function(e){
		checkPlatform();
  	});
  	$("#platform").keyup(function(e){
		checkPlatform();
  	});
  	$("#barcode").keyup(function(e){
		checkPlatform();
  	});
  	$(document).keypress(function(e) {
	    if(e.which == 13) {
	        checkPlatform();
	    }
	});
  	$("#submit-button").click(function(e){
  		var barcode = $("#barcode").val();
  		var plat = $("#platform").val();
  		var start_date = $("#start_date").val();
  		var end_date = $("#end_date").val();
  		var date1 = +new Date(start_date);
  		var date2 = +new Date(end_date);
  		console.log('date1' + date1);
  		console.log('date2' + date2);
  		// if(plat == ""){
  		// 	var x = document.getElementById("snackbar");
		  //   x.className = "show";
		  //   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		  //   $("#msg_response").html("Platform / Branch Name cannot be empty");
		  //   return false;
  		// }
  		// if(barcode == ""){
  		// 	var x = document.getElementById("snackbar");
		  //   x.className = "show";
		  //   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		  //   $("#msg_response").html("Barcode or SKU cannot be empty");
		  //   return false;
  		// }
  		if(date1 > date2){
  			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		    $("#msg_response").html("Start Date cannot be greater than End Date");
		    return false;
  		}
  		checkPlatform();
  	});
  	function checkPlatform(){
  		var xcode = $("#platform").val();
  		var barcode = $("#barcode").val();
  		
  			
	  	$.get('{{url('get')}}/platform?code='+xcode,function(data){
	    	var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		    $("#msg_response").html(data.msg);
		    // if(data.status == 0){
		    // 	$("#submit-button").prop('disabled','true');
		    // 	return false;
		    // }else{
		    // 	if(barcode=="")
		    // 		{$("#submit-button").prop('disabled','true');
		  		// 	return false;
		  		// }else{
			   //  	$("#submit-button").removeAttr('disabled');
			   //  	return true;
		  		// }
		    // }
	    });
  	};

  	var path = "{{url('/branch/search/autocomplete')}}";
  	$('#platform').typeahead({
  		source:function(query,process){
  			return $.get(path, {query:query},function(data){
  				return process(data);
  			});
  		}
  	});
  	var skupath = "{{url('/sku/search/autocomplete')}}";
  	$('#barcode').typeahead({
  		source:function(query,process){
  			return $.get(skupath, {query:query},function(data){
  				return process(data);
  			});
  		}
  	});
});
</script>
<style >
div.dt-buttons {
    float: right;
}
</style>
@stop