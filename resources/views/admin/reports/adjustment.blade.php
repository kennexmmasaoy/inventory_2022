@extends('layouts.admin-default')
@section('content')
<div id="snackbar">
	<p id="msg_response" class="msg_response"></p>
</div>
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>Reports : Adjustments</h3>
			</div>
		</div>
		<div class="container-row">
			<div class="col-md-4">
				<div class="row">

					<div id="platformGroup" class="form-group">
					 	<label for="platform" class=" col-form-label">Platform : </label>
					 	<input type="text" class="form-control" id="platform" />
					</div>	
					<div id="skuGroup" class="form-group">
					 	<label for="sku" class=" col-form-label">SKU : </label>
					 	<input type="text" class="form-control" id="sku" />
					  	<div class="invalid-feedback"></div>
					</div>	
					<div class="form-group ">
					 	<label for="qty" class=" col-form-label">Quantity : </label>
					 	<input type="text" class="form-control" id="quantity" />
					 	<input type="number" class="form-control hidden" id="itemId" />
					 	<small><strong>NOTE : </strong>Number of Items to be deducted</small>
					</div>	
					<div class="form-group">
						<a class="btn btn-success pull-right" id="page-reload" /><i class="fa fa-retweet"></i></a>
						<input type="button" class="btn btn-warning pull-right" id="adjust-item" value="Adjust" />
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<table id="productList">
					<thead>
						<tr>
							<th>SKU</th>
							<th>Barcode</th>
							<th>Site</th>
							<th>Quantity</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($productList as $item)
						<tr>
							<td>{{$item->sku}}</td>
							<td>{{$item->barcode}}</td>
							<?php 
								$plat_info = App\Http\Model\PlatformModel::where('id',$item->platform_id)->select('platform_name')->first();
							?>
							<td>{{$plat_info['platform_name']}}</td>
							<td>{{$item->finalQuantity}}</td>
							<td><a href="#" id="btn_update"  data-id="{{$item->platform_product_list_id}}" data-quant="{{$item->finalQuantity}}" data-site="{{$plat_info['platform_name']}}" data-barcode="{{$item->sku}}">Adjust Quantity</a></td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</section>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#productList").DataTable({
			dom: 'lfrtip',
    		"aoColumns": [ {"bSearchable": true},{"bSearchable": true}, {"bSearchable": true},{"bSearchable": false},{"bSearchable": false} ],
		});
		$('#productList').on('click',"a[id='btn_update']",function(){
		    var barcode = $(this).closest(this).data('barcode');
		    var platform = $(this).closest(this).data('site');
		    var id = $(this).closest(this).data('id');
		    var qty = $(this).closest(this).data('quant');
		    $('#platform').val(platform);
		    $('#sku').val(barcode);
		    $('#itemId').val(id);
		    $('#quantity').val(qty);
		});
	});
	$(function() {
		$("#adjust-item").on('click',function(e){
			var platform = $("#platform").val();
			var sku = $("#sku").val();
			var itemId = $("#itemId").val();
			var qty = $("#quantity").val();
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			if(!platform){
				$("#msg_response").html("Platform / Site / Branch cannot be empty");
				return false;
			};
			if(!sku){
				$("#msg_response").html("SKU cannot be empty");
				return false;
			};
			var dataVal = {
				platform : platform,
				sku : sku,
				id : itemId,
				qty : qty
			};
			$.get('{{url('adjust')}}/item/quantity',dataVal,function(data){
				console.log(data);
			});
		});
		$("#sku").on('blur',function(e){
			checkIfExsit();
		});
		$("#platform").on('blur',function(e){
			checkIfExsit();
		});
		$("#quantity").on('focusin',function(e){
			checkIfExsit();
		});
		$("#quantity").on('keyup',function(e){
			var platform_name = $("#platform").val();
			var sku = $("#sku").val();
			var qty = $("#quantity").val();
			var dataVal = {
				platform : platform_name,
				sku : sku,
				qty : qty
			}
			$.get('{{url('check')}}/item/quantity',dataVal,function(ret){
				if(ret.status == 1){
					$("#adjust-item").removeAttr('disabled');
				}else{
					$("#adjust-item").prop('disabled','true');
				}
				console.log(ret);
			});
		});
		$('#page-reload').click(function() {
		    location.reload();
		});
		function checkIfExsit(){
			var platform_name = $("#platform").val();
			var sku = $("#sku").val();
			var data = {
				platform : platform_name,
				sku : sku
			};
			$.get('{{url('check')}}/exist/list',data,function(data){
				if(data.status == '0'){
					$('#itemId').val(data.ids);
					$(".invalid-feedback").html(data.msg);
					$("#adjust-item").prop('disabled','true');
					$("#skuGroup").removeClass('has-success');
					$("#platformGroup").removeClass('has-success');
					$("#quantity").prop('disabled','true');
					console.log(data);
				}else{
					$("#adjust-item").removeAttr('disabled');
					$('#itemId').val(data.ids);
					$(".invalid-feedback").html(data.msg);
					$("#skuGroup").addClass('has-success');
					$("#platformGroup").addClass('has-success');
					$("#quantity").removeAttr('disabled');
					console.log(data);
				}
		    	
			});
		}
		var path = "{{url('/sku/search/autocomplete')}}";
	  	$('#sku').typeahead({
	  		source:function(query,process){
	  			return $.get(path, {query:query},function(data){
	  				return process(data);
	  			});
	  		}
	  	});
	  	var skupath = "{{url('/branch/search/autocomplete')}}";
	  	$('#platform').typeahead({
	  		source:function(query,process){
	  			return $.get(skupath, {query:query},function(data){
	  				return process(data);
	  			});
	  		}
	  	});
	});
</script>

@endsection