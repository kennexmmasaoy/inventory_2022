@extends('layouts.login')
@section('content')
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class=" validate-form" action="{{action('LoginController@loginAuth')}}">
					<span class="login100-form-title p-b-48">
						<img class="img-responsive" style="max-width:150px" src="{{url('assets-admin\login\images\icons\UnisilverTimeLogo.png')}}" >
					</span>
					@if(Session::has('warning'))
						<div class="alert alert-dismissible alert-warning">
					 		 <button type="button" class="close" data-dismiss="alert">&times;</button>
					 		 <h4 class="alert-heading">Warning!</h4>
					  		<p class="mb-0">{{Session::get('warning')}}</p>
						</div>
					@endif	
					<div class="wrap-input100">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="pass">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
@stop