<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="col-xs-12 head_col_div">
  <ul class="list-inline pull-right head_col">
    <li><a href="{{url('/')}}">Product List</a></li>
    <li><a href="{{url('/registration')}}">Register</a></li>
    <li><a href="{{url('/signin')}}">Sign In</a></li>
  </ul>
</div>
<div class="head_col_div head_col">
  <h4 class="head_col_1">INVENTORY SYSTEM</h4>
  <p class="head_col_1">Chronotron Inc. (UnisilverTime)</p> 
</div>

@yield('content')
<div class="col-xs-12 text-center">
  <hr>
  <span class="text-center">&#169; <?php echo date("Y"); ?> Chronotron Inc. All Rights Reserved.</span>
</div>
</body>
</html>
