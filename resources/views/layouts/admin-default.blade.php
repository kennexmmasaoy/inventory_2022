<!DOCTYPE html>
<html lang="en">
  <head>
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Inventory System</title>

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>

    <link rel="stylesheet" href="{{asset('assets-admin/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets-admin/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets-admin/css/style-responsive.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdn.anychart.com/css/7.14.3/anychart-ui.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">

    
  </head>

  <body>
<script type="text/javascript">
  var sess = "{{Session::has('isLogin')}}";
  if(!sess){
    window.location = "{{url('login')}}";
  }
</script>
  <section id="container" >
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="#" class="logo"><b>Inventory System Admin</b></a>
            <!--logo end-->
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="{{url('/logout')}}"><i class="fa fa-user fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;logout</a></li>
              </ul>
            </div>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>

          <div id="sidebar"><br><br><br>
              <!-- sidebar menu start-->
              <ul id="menu">
                  <h5 style="margin-top:20px;font-size:16px;"><a href="{{url('/dashboard')}}" style="color:#fff;">Admin Dashboard</a></h5>
                  
                  <li>
                      <a href="{{url('/dashboard')}}">
                          <i class="fa fa-tachometer-alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <li>
                      <a href="{{url('productspecs/watch_info')}}">
                          <i class="fa fa-info-circle"></i>
                          <span>Watch Info</span>
                      </a>
                  </li>
                  <li>
                      <a href="javascript:void(0);" class="clickme">
                          <i class="fa fa-desktop"></i>
                          <span>Product Specification</span>
                      </a>
                      <ul class="sub-level-2 list-unstyled" style="display:none">
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/band_materials')}}#Products">Band Material</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/case_materials')}}#Products">Case Material</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/case_thickness')}}#Products">Case Thickness</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/dial')}}#Products">Dial</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/case_width')}}#Products">Case Width</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/clasp_type')}}#Products">Clasp Type</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/color')}}#Products">Color</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/face_height')}}#Products">Face Height</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/wrist_circumference')}}#Products">Wrist Circumference</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/water_resistance')}}#Products">Water Resistance</a></li>
                          <li><a class="dropdown-class-name2 sub_a" href="{{url('productspecs/brand')}}#Products">Brand</a></li>
                      </ul>
                  </li>
                  <!-- <li>
                      <a href="{{url('transmittal/transmittal_info')}}">
                          <i class="fa fa-info-circle"></i>
                          <span>Transmittal Info</span>
                      </a>
                  </li>
                  <li>
                      <a href="javascript:void(0);" class="clickme">
                          <i class="fa fa-book"></i>
                          <span>Transmittal</span>
                      </a>
                      <ul class="sub-level-4 list-unstyled" style="display:none">
                        <li><a class="dropdown-class-name4 sub_a" href="{{url('transmittal/area')}}#transmittal">Area</a></li>
                          <li><a class="dropdown-class-name4 sub_a" href="{{url('transmittal/branch')}}#transmittal">Branch</a></li>
                          <li><a class="dropdown-class-name4 sub_a" href="{{url('transmittal/branchtype')}}#transmittal">Branch Type</a></li>
                          <li><a class="dropdown-class-name4 sub_a" href="{{url('transmittal/chronotype')}}#transmittal">Chrono Type</a></li>
                          <li><a class="dropdown-class-name4 sub_a" href="{{url('transmittal/promo')}}#transmittal">Promo</a></li>
                      </ul>
                  </li> -->
                  @if(Session::get('isLogin') == 16)
                  <li>
                      <a href="javascript:void(0);" class="clickme">
                          <i class="fa fa-wrench"></i>
                          <span>System CRUD</span>
                      </a>
                      <ul class="sub-level-5 list-unstyled" style="display:none">
                        <li><a class="dropdown-class-name5 sub_a" href="{{url('transaction/platform')}}#transaction">Platform</a></li>
                        <li><a class="dropdown-class-name3 sub_a" href="{{url('user/useraccount')}}#transaction">User Accounts</a></li>
                        <li><a class="dropdown-class-name3 sub_a" href="{{url('user/userofficeinfo')}}#transaction">User Office Info</a></li>
                      </ul>
                  </li>
                  <li>
                      <a href="javascript:void(0);" class="clickme">
                          <i class="fa fa-cogs"></i>
                          <span>Settings</span>
                      </a>
                      <ul class="sub-level-6 list-unstyled" style="display:none">
                        <li><a class="dropdown-class-name3 sub_a" href="{{url('user/userpriviliges')}}#Settings">User Privileges</a></li>
                      </ul>
                  </li>
                  @endif
              </ul><br><br>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      

    @yield('content')

  <!-- js placed at the end of the document so the pages load faster -->
  <script type="text/javascript" src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
   <!-- DataTables JavaScript -->
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script> 
 
            <script>
                $(document).ready(function () {

                $('#menu li > a').click(function (e) {

                    if ($(this).next('ul').length > 0) {

                        e.preventDefault();

                        var subNav = $(this).next('ul');

                        if (subNav.is(':visible')) {
                            subNav.hide('normal')
                            $(this).removeClass("selected");
                        } else {
                            $('#menu ul:visible').hide('normal');
                            subNav.slideDown('normal');
                            $("a.selected").removeClass("selected");
                            $(this).addClass("selected");
                        }
                    }
                });
            });
            $(document).ready(function () {
            if (window.location.href.indexOf("#Upload") > -1) {
                $('.dropdown-class-name1').closest(".sub-level-1").css("display","block");
            }
        });
            $(document).ready(function () {
            if (window.location.href.indexOf("#Products") > -1) {
                $('.dropdown-class-name2').closest(".sub-level-2").css("display","block");
            }
        });
            $(document).ready(function () {
            if (window.location.href.indexOf("#User") > -1) {
                $('.dropdown-class-name3').closest(".sub-level-3").css("display","block");
            }
        });
            $(document).ready(function () {
            if (window.location.href.indexOf("#transmittal") > -1) {
                $('.dropdown-class-name4').closest(".sub-level-4").css("display","block");
            }
        });
            $(document).ready(function () {
            if (window.location.href.indexOf("#transaction") > -1) {
                $('.dropdown-class-name5').closest(".sub-level-5").css("display","block");
            }
        });
            $(document).ready(function () {
            if (window.location.href.indexOf("#Settings") > -1) {
                $('.dropdown-class-name6').closest(".sub-level-6").css("display","block");
            }
        });
          </script>

</html>
