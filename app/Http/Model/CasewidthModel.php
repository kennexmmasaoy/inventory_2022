<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class CasewidthModel extends Model
{
   protected $table = "case_width";
    protected $fillable = [
    	'size',
    	'lenght_indicator',
    ];
    protected $dateFormat = 'U';
}
