<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PlatformProductListHistoryModel extends Model
{
    //
    protected $table = 'platform_product_list_history';
    protected $fillable = [
    	'document_id',
    	'PPL_id',
    	'quantity',
    	'totalQuantity',
    	'type',
    ];
    protected $dateFormat = 'U';
}
