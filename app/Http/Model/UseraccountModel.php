<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UseraccountModel extends Model
{
     protected $table = 'users';
	protected $fillable = [
	'uid',
	'username',
	'password',
	'user_priv',
	];

	protected $dateFormat = 'U';

	function userprivdisplay(){
		$Uaccount = DB::table('users AS ua')
		->leftjoin('user_privileges','ua.user_priv','=','user_privileges.id')
		->select('*','ua.id as useraccount_id')
		->orderBy('ua.id','desc')
		->get();
		return $Uaccount;
	}
}
