<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ZipModel extends Model
{
   protected $table = "zip_uploads";
    protected $fillable = [
    	'zip_name',
    	'size',
    	'zip_uid',
    	'status',
        'watch_info_id',
    ];
    protected $dateFormat = 'U';

    /*
	* insert zip in the database and get inserted ID to be a foreign key in the other table
	* @params $title varchar
	* @params $input varchar
	* @params $real_name varchar
	* @params $imagedest varchar
	*/
    function insertZip($size, $real_name, $zipdest,$b,$channel){
    	$data = array(
			'zip_name' => $real_name,
			'size' => $size,
			'url' => $zipdest,
			'status' => 1,
			'watch_info_id' => $b,
			'channel' => $channel,

		);
		$j = DB::table('zip_uploads')->insertGetId($data);
		return $j;
    }
    /*
	* update zip in the database and get inserted ID to be a foreign key in the other table
	* @params $title varchar
	* @params $input varchar
	* @params $real_name varchar
	* @params $imagedest varchar
	*/
    function updateZip($size, $real_name, $zipdest,$b,$channel){
    	$data = array(
			'zip_name' => $real_name,
			'size' => $size,
			'url' => $zipdest,
			'status' => 1,
			'watch_info_id' => $b,
			'channel' => $channel,
		);
		$j = DB::table('zip_uploads')->where('id')->update($data);
		return $j;
    }
    /*
	* get the a single zip
	* @params $pids integer
	*/
    function getZip($pids){
    	$gZip = DB::table('zip_uploads')->where('id',$pids)->first();
    	return $gZip;
    }
}
