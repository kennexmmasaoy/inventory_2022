<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WristcircumferenceModel extends Model
{
   protected $table = "max_wrist_circumference";
    protected $fillable = [
    	'size',
    	'lenght_indicator',
    ];
    protected $dateFormat = 'U';
}
