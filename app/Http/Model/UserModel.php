<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserModel extends Model
{
    protected $table = 'user_office_info';
	protected $fillable = [
	'office_id_number',
	'firstname',
	'lastname',
	'middlename',
	'department_id',
	'contact_number',
	'status'
	];

	protected $dateFormat = 'U';
}
