<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class PlatformModel extends Model
{
    protected $table = "platform";
    protected $fillable = [
    	'platform_name',
    	'quantity',
    	'image_id',
    ];
    protected $dateFormat = 'U';

    function platformDisplay(){
    	$platforminfo = DB::table('platform AS pid')
    	->leftjoin('image','pid.image_id','=','image.id')
    	->select('*','pid.id as platform_id')
    	->get();
    	return $platforminfo;
    }
}
