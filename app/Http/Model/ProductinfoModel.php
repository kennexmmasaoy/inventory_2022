<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ProductinfoModel extends Model
{
    protected $table = "band_material";
    protected $fillable = [
    	'material',
    ];
    protected $dateFormat = 'U';
}
