<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class WatchlistModel extends Model
{
    protected $table = "watchlist";
    protected $fillable = [
    	
    ];
    protected $dateFormat = 'U';
}
