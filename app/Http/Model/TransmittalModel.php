<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransmittalModel extends Model
{
    protected $table = "transmittal_info";
    protected $fillable = [
    	'area',
    	'out_of_delivery',
    	'location',
    	'branch',
    	'branch_type',
    	'chrono_type',
        'promo_type',
        'm_tarpaulin',
    	'm_generic',
    	'm_paper_signage',
    	's_tarpaulin',
    	's_generic',
    	's_paper_signage',
    ];
    protected $dateFormat = 'U';

    public function TransInfoDisplay($search=''){
    	$transinfo = DB::table ('transmittal_info AS tid')
    	->leftjoin('area','tid.area','=','area.id')
    	->leftjoin('branch','tid.branch','=','branch.id')
    	->leftjoin('branch_type','tid.branch_type','=','branch_type.id')
    	->leftjoin('chrono_type','tid.chrono_type','=','chrono_type.id')
        ->leftjoin('promo','tid.promo_type','=','promo.id')
    	->select('*','tid.id as trans_id')
        ->orderBy('trans_id','desc')
        ->where('area_name','like','%'.$search.'%')
    	->paginate(10);
    	return $transinfo;
    }
}
