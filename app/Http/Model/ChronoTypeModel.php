<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ChronoTypeModel extends Model
{
      protected $table = "chrono_type";
    protected $fillable = [
    	'chrono_type_name',
    ];
    protected $dateFormat = 'U';
}
