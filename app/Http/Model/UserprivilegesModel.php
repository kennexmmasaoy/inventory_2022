<?php
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserprivilegesModel extends Model
{
    protected $table = 'user_privileges';
	protected $fillable = [
	'p_name',
	'status'
	];

	protected $dateFormat = 'U';
}
