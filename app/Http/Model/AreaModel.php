<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model
{
    protected $table = "area";
    protected $fillable = [
    	'area_number',
    	'area_name',
    ];
    protected $dateFormat = 'U';
}
