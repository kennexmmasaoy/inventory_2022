<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class CasethicknessModel extends Model
{
    protected $table = "case_thickness";
    protected $fillable = [
    	'size',
    	'lenght_indicator',
    ];
    protected $dateFormat = 'U';
}
