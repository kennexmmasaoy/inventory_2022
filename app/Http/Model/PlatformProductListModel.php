<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PlatformProductListModel extends Model
{
    protected $table = 'platform_product_list';
    protected $fillable = [
    	'platform_id',
    	'sku',
    	'barcode',
    	'finalQuantity'
    ];
    protected $dateFormat = 'U';
}
