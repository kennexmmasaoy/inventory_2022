<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class FaceheightModel extends Model
{
    protected $table = "face_height";
    protected $fillable = [
    	'size',
    	'lenght_indicator',
    ];
    protected $dateFormat = 'U';
}
