<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class BranchTypeModel extends Model
{
    protected $table = 'branch_type';
    protected $fillable = [
    	'branch_type_name',
    ];
    protected $dateFormat = 'U';
}
