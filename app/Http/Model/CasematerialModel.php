<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class CasematerialModel extends Model
{
    protected $table = "case_material";
    protected $fillable = [
    	'material',
    ];
    protected $dateFormat = 'U';
}
