<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ColorModel extends Model
{
    protected $table = "color";
    protected $fillable = [
    	'color_name',
    	'color_hex',
    	'color_rgb',
    ];
    protected $dateFormat = 'U';
}

// function color(){
// 	return $this->belongsTo('App\Http\Model\WatchinfoModel', 'id');
// }