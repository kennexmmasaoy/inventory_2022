<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class WaterresistanceModel extends Model
{
    protected $table = "water_resistance";
    protected $fillable = [
    	'depht',
    ];
    protected $dateFormat = 'U';
}
