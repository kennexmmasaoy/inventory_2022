<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
     protected $table = "brand";
    protected $fillable = [
    	'brand_name',
    ];
    protected $dateFormat = 'U';
}