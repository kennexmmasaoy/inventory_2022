<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class WatchinfoModel extends Model
{
    protected $table = "watch_info";
    protected $fillable = [
        'barcode',
    	'sku',
    	'product_name',
    	'product_description',
    	'product_description_2',
    	'color',
    	'dial',
        'brand',
    	'case_material',
    	'face_height_with_bezel',
    	'case_width',
    	'case_thickness',
    	'max_wrist_circumference',
    	'clasp_type',
    	'water_resistance',
    	'other_features',
    	'warranty',
    	'price',
        'image_id',
    ];
    protected $dateFormat = 'U';

        function watchinfoDisplay($search=''){
        $watchinfo = DB::table('watch_info AS wid')
        ->leftjoin('color','wid.color','=','color.id')
        ->leftjoin('case_material','wid.case_material','=','case_material.id')
        ->leftjoin('dial','wid.dial','=','dial.id')
        ->leftjoin('brand','wid.brand','=','brand.id')
        ->leftjoin('case_thickness','wid.case_thickness','=','case_thickness.id')
        ->leftjoin('case_width','wid.case_width','=','case_width.id')
        ->leftjoin('clasp_type','wid.clasp_type','=','clasp_type.id')
        ->leftjoin('water_resistance','wid.water_resistance','=','water_resistance.id')
        ->leftjoin('face_height','wid.face_height_with_bezel','=','face_height.id')
        ->leftjoin('max_wrist_circumference','wid.max_wrist_circumference','=','max_wrist_circumference.id')
        ->select('*','wid.id as info_id')
        ->orderBy('brand.brand_name','desc')
        ->where('product_name','like','%'.$search.'%')
        ->orwhere('sku','like','%'.$search.'%')
        ->orwhere('product_description','like','%'.$search.'%')
        ->orwhere('color_name','like','%'.$search.'%')
        ->orwhere('dial_name','like','%'.$search.'%')
        ->orwhere('type','like','%'.$search.'%')
        ->orwhere('depht','like','%'.$search.'%')
        ->orwhere('price','like','%'.$search.'%')
        ->orwhere('brand_name','like','%'.$search.'%')
        ->orwhere('face_height.size','like','%'.$search.'%')
        ->orwhere('case_width.size','like','%'.$search.'%')
        ->orwhere('case_thickness.size','like','%'.$search.'%')
        ->paginate(10);
        return $watchinfo;
       
    }
}


