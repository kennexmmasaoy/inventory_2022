<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PromoModel extends Model
{
     protected $table = 'promo';
    protected $fillable = [
    	'promo_name',
    ];
    protected $dateFormat = 'U';
}
