<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransactionModel extends Model
{
    protected $table = "transaction";
    protected $fillable = [
    	'admin',
    	'type',
    	'platform_id',
    	'uid',
    	'transaction_qty',
    ];
    protected $dateFormat = 'U';

    function transactionDisplay(){
    	$transacinfo = DB::table('transaction AS tid')
    	->leftjoin('platform','tid.platform_id','=','platform.id')
    	->leftjoin('users','tid.uid','=','users.id')
    	->select('*','tid.id as transac_id')
    	->get();
    	return $transacinfo;
    }
}
