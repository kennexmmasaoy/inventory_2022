<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DocumentModel extends Model
{
    protected $table = "platform_product_list_document";
    protected $fillable = [
    	'id',
    	'document_name',
    	'user_id',
    	'status',
    	'type',
    	'selectedDate'
    ];
    protected $dateFormat = 'U';
}
