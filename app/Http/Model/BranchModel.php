<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class BranchModel extends Model
{
    protected $table = 'branch';
    protected $fillable = [
    	'branch_name',
    ];
    protected $dateFormat = 'U';
}
