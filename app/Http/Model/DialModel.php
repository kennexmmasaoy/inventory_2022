<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DialModel extends Model
{
    protected $table = "dial";
    protected $fillable = [
    	'dial_name',
    ];
    protected $dateFormat = 'U';
}
