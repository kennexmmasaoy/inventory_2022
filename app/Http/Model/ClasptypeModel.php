<?php

namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;

class ClasptypeModel extends Model
{
    protected $table = "clasp_type";
    protected $fillable = [
    	'type',
    ];
    protected $dateFormat = 'U';
}
