<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ImageModel extends Model
{
    protected $table = 'image';
	protected $fillable = ['original_name','savename','url','md5','status','uid_uploader','size'];

	protected $dateFormat = 'U';


	/*
	* insert image in the database and get inserted ID to be a foreign key in the other table
	* @params $title varchar
	* @params $input varchar
	* @params $real_name varchar
	* @params $imagedest varchar
	*/
    function insertImg($size, $input, $real_name, $imagedest){
    	$data = array(
			'original_name' => $real_name,
			'savename' => $input,
			'url' => $imagedest,
			'md5' => md5($real_name),
			'updated_at' => time(),
			'created_at' => time(),
			'status' => 1,
		);
		$j = DB::table('image')->insertGetId($data);
		return $j;
    }
    /*
	* update image in the database and get inserted ID to be a foreign key in the other table
	* @params $title varchar
	* @params $input varchar
	* @params $real_name varchar
	* @params $imagedest varchar
	*/
    function updateImg($pid, $size, $input, $real_name, $imagedest){
    	$data = array(
			'original_name' => $real_name,
			'savename' => $input,
			'url' => $imagedest,
			'md5' => md5($real_name),
			'updated_at' => time(),
			'created_at' => time(),
			'status' => 1,
		);
		$j = DB::table('image')->where('id',$pid)->update($data);
		return $j;
    }
    /*
	* get the a single image
	* @params $pids integer
	*/
    function getImg($pids){
    	$gImg = DB::table('image')->where('id',$pids)->first();
    	return $gImg;
    }
}
