<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\PlatformModel;
use App\Http\Model\DocumentModel;
use App\Http\Model\WatchInfoModel;
use App\Http\Model\PlatformProductListModel;
use App\Http\Model\PlatformProductListHistoryModel;
use Carbon;

class ReportsController extends Controller
{
    public function searchItem(){
    	$barcode= \Request::get('barcode');
    	$startDate= \Request::get('start_date');
    	$endTime= \Request::get('end_date');
    	$platform= \Request::get('platform');
    	$reportType= \Request::get('reportType');
        $endDate = Carbon::parse($endTime)->endOfDay();
    	
    	$stock = PlatformModel::join('platform_product_list','platform.id','=','platform_product_list.platform_id');
    								
    	if($reportType==1){
    		$stock->join('platform_product_list_history','platform_product_list.platform_product_list_id','=','platform_product_list_history.PPL_id')
					->select('platform.platform_name','platform.id','platform_product_list_history.PPL_id','platform_product_list_history.document_id','platform_product_list_history.quantity','platform_product_list_history.type as transType','platform_product_list.sku','platform_product_list.barcode','platform_product_list_history.created_at','platform_product_list_history.totalQuantity')
                    ->where('type','>','-1');
			if(\Request::has('barcode')){
				$stock->where(function($que) use ($barcode){
                    $que->orWhere('platform_product_list.barcode',$barcode)->orWhere('platform_product_list.sku',$barcode);
                });
			}
			if(\Request::has('platform')){
				$stock->where('platform_product_list.platform_id',$platform);
			}
			if(\Request::has('start_date') || \Request::has('end_date')){
				if($startDate == $endDate){
					$stock->where('platform_product_list_history.created_at',strtotime($startDate));
				}else{
					$stock->whereBetween('platform_product_list_history.created_at', array(strtotime($startDate), strtotime($endDate)));
				}
			}
    	}else{

    		$stock->select('platform.platform_name','platform_product_list.finalQuantity','platform_product_list.barcode','platform_product_list.sku');
    		if($barcode){
				$stock->where(function($que) use ($barcode){
                    $que->orWhere('platform_product_list.barcode','like','%'.$barcode.'%')
                        ->orWhere('platform_product_list.sku','like','%'.$barcode.'%');
                });
			}
    	}
    	$search_info = array(
    		'barcode' => $barcode,
    		'startDate' => $startDate,
    		'endDate' => $endTime,
    		'platform' => $platform,
    		'reportType' => $reportType
    	);
    	$reportStockCard = $stock->get();
        // dd($search_info);
    	$plat = PlatformModel::all();
    	//start time = 1529971200
    	//end time = 1530057600
    	// dd(strtotime($startDate));
    	return view('admin.reports.search',compact('plat','reportStockCard','search_info'));
    }
    public function stockEvaluation(){
    	$platform= \Request::get('platform');
    	$barcode= \Request::get('barcode');
    	$startDate= \Request::get('start_date');
    	$endTime= \Request::get('end_date');
         $endDate = Carbon::parse($endTime)->endOfDay();
        $getInfo = PlatformProductListModel::select('platform_product_list.barcode','platform_product_list.platform_id', 'platform_product_list.sku','platform_product_list.finalQuantity');
            // ->join('platform_product_list_history','platform_product_list_history.PPL_id','=','platform_product_list.platform_product_list_id')
            // ->join('platform_product_list_document','platform_product_list_history.document_id','=','platform_product_list_document.id');
        if(\Request::has('platform')){
            $searchPlat = PlatformModel::where('platform_name',$platform)->first();
            if($searchPlat->id > 0){
                $getInfo->where('platform_product_list.platform_id',$searchPlat->id);
            }
        }
        // if(\Request::has('start_date') || \Request::has('end_date')){
		// 	if($startDate == $endDate){
		// 		$getInfo->where('platform_product_list_history.created_at',strtotime($startDate));
		// 	}else{
		// 		$getInfo->whereBetween('platform_product_list_history.created_at', array(strtotime($startDate), strtotime($endDate)));
		// 	}
		// }
		if(\Request::has('barcode')){
			$getInfo->where(function($que) use ($barcode){
                $que->orWhere('platform_product_list.barcode',$barcode)->orWhere('platform_product_list.sku',$barcode);
            });
		}

    	$info = $getInfo
                    // ->where('platform_product_list_document.status','1')
                    // ->where('platform_product_list_history.type','1')
                    ->get();
    	$stock_eval_info = array(
    		'platform' => $platform,
    		'barcode' => $barcode,
    		'start_date' => $startDate,
    		'endDate' => $endTime
    	);
    	return view('admin.reports.stock_evaluation',compact('stock_eval_info','info'));
    }
    public function getPlatform(){
    	$code= \Request::get('code');
    	$start_date= \Request::get('start_date');
    	$endTime= \Request::get('end_date');
    	$barcode= \Request::get('barcode');
         $end_date = Carbon::parse($endTime)->endOfDay();

    	$data = PlatformModel::where('platform_name',$code)->first();
    	if($data == ""){
    		$plat = array(
    			'msg' => 'Branch / Platform Cannot be found',
    			'status' => 0
    		);
    	}else{
    		$plat = array(
    			'msg' => 'Branch / Platform Available',
    			'status' => 1
    		);
    	}
    	return \Response::json($plat);
    }
    public function salesAnalysis(){
    	$platform= \Request::get('platform');
    	$barcode= \Request::get('barcode');
    	$startDate= \Request::get('start_date');
    	$endTime= \Request::get('end_date');
         $endDate = Carbon::parse($endTime)->endOfDay();

    	$getInfo = WatchInfoModel::select('platform_product_list.barcode', 'platform_product_list.sku','watch_info.product_description','watch_info.price','platform_product_list_history.quantity','platform_product_list.platform_id','platform_product_list_history.created_at')
    		->join('platform_product_list','watch_info.barcode','=','platform_product_list.barcode')
    		->join('platform_product_list_history','platform_product_list_history.PPL_id','=','platform_product_list.platform_product_list_id')
            ->join('platform_product_list_document','platform_product_list_document.id','=','platform_product_list_history.document_id');
        if(\Request::has('platform')){
            $searchPlat = PlatformModel::where('platform_name',$platform)->first();
            if($searchPlat->id != '28'){
                $getInfo->where('platform_product_list.platform_id',$searchPlat->id);
            }
        }
         
		if(\Request::has('start_date') || \Request::has('end_date')){
			if($startDate == $endDate){
				$getInfo->where('platform_product_list_history.created_at',strtotime($startDate));
			}else{
				$getInfo->whereBetween('platform_product_list_history.created_at', array(strtotime($startDate), strtotime($endDate)));
			}
		}
		if(\Request::has('barcode')){
			$getInfo->where(function($que) use ($barcode){
                $que->orWhere('platform_product_list.barcode',$barcode)->orWhere('platform_product_list.sku',$barcode);
            });
		}
    	$info = $getInfo->where('platform_product_list_history.type','0')
                    ->where('platform_product_list_document.type','0')->where('platform_product_list_document.status','1')->get();

    	$stock_eval_info = array(
    		'platform' => $platform,
    		'barcode' => $barcode,
    		'start_date' => $startDate,
    		'endDate' => $endTime
    	);
    	return view('admin.reports.sales_analysis',compact('stock_eval_info','info'));
    }

    public function branchAutoComplete(Request $request){
        $term = $request->input('query');
        $results = array();
        
        $queries = PlatformModel::
            where('platform_name', 'LIKE', '%'.$term.'%')
            ->take(5)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'name' => $query->platform_name ];
        }
    return \Response::json($results);
    }
    public function SkuAutoComplete(Request $request){
        $term = $request->input('query');
        $results = array();
        
        $queries = WatchInfoModel::
            where('sku', 'LIKE', '%'.$term.'%')
            ->take(5)->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'name' => $query->sku ];
        }
    return \Response::json($results);
    }
    public function adjustProduct(){
        $productList = PlatformProductListModel::select('sku','barcode','platform_id','finalQuantity','platform_product_list_id')->get();
        return view('admin.reports.adjustment',compact('productList'));
    }
    public function checkExist(){
        $platform= \Request::get('platform');
        $sku = \Request::get('sku');
        $pm = PlatformModel::where('platform_name',$platform)->select('id')->first();
        $i = PlatformProductListModel::where('platform_id',$pm->id)->where('sku',$sku)->first();
        if( $i){
            $ret = array(
                'status' => 1,
                'msg' => "Found and Available",
                'ids' => $i->platform_product_list_id
            );
        }else{
            $ret = array(
                'status' => 0,
                'msg' => "Unable to find."
            );
        }
        return $ret;
    }
    public function checkQuantity(){
        $platform= \Request::get('platform');
        $sku = \Request::get('sku');
        $qty = \Request::get('qty');
        $pm = PlatformModel::where('platform_name',$platform)->select('id')->first();
        $pplm = PlatformProductListModel::where('platform_id',$pm->id)->where('sku',$sku)->select('finalQuantity')->first();
        if($pplm->finalQuantity >= $qty){
            $ret = array(
                'status' => 1,
                'msg' => "The input quantity is less than or equal to the available quantity"
            );
        }else{
            $ret = array(
                'status' =>0,
                'msg' => "The input quantity is greater than the available quantity"
            );
        }
        return $ret;
    }
    function createDocumentTransfer($id,$transfer_type,$status='',$type){
        $todayDate = Carbon::now()->toDateString();
        if($id=='new'){
            $checkData = DocumentModel::where('type',$transfer_type)->orderBy('id','desc')->first();
            if($checkData){
                $checkData->id+=1;
                $data = array(
                    'id' => $checkData->id,
                    'user_id' => session()->get('isLogin'),
                    'status' => $status,
                    'selectedDate' => $todayDate,
                    'type' => $type 
                );
                $doc_id = DocumentModel::create($data);
                $doc = $checkData->id;
            }else{
                $data = array(
                    'id' => '1',
                    'user_id' => session()->get('isLogin'),
                    'status' => $status,
                    'selectedDate' => $todayDate,
                    'type' => $type 
                );
                $doc_id = DocumentModel::create($data);
                $doc = $data['id'];
            }
        }else{
            $doc = $id;
        }
        return $doc;
    }
    public function adjustItemQuantity(){
        $platform= \Request::get('platform');
        $sku= \Request::get('sku');
        $id= \Request::get('id');
        $qty= \Request::get('qty');
        $transfer_type = '2';
        $pplm = PlatformProductListModel::where('platform_product_list_id',$id);
        $platform = PlatformModel::where('platform_name',$platform)->select('id')->first();
        $doc = self::createDocumentTransfer('new',$transfer_type,1,2);

        $adjustmentData = array(
            'platform_id' => $platform->id,
            'document_id' =>  $doc,
            'quantity' => $qty,
            'type' => 2,
            'PPL_id' => $id
        );
        PlatformProductListHistoryModel::create($adjustmentData);
        $item = $pplm->first();
        $dataQty = array(
            'finalQuantity' =>  ($item->finalQuantity - $qty)
        );
        $i = $pplm->update($dataQty);
        // if($i){

        // }else{

        // }
    }
}
