<?php

namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;
use DB;
use App\Http\Model\AreaModel;
use App\Http\Model\BranchModel;
use App\Http\Model\BranchTypeModel;
use App\Http\Model\ChronoTypeModel;
use App\Http\Model\TransmittalModel;
use App\Http\Model\PromoModel;

class TransmittalController extends Controller
{
    public function area(){
    	$area_list = AreaModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.area',compact('area_list'));
    }

    public function savearea(Request $request){
    	$data = $request->all();
    	$area_model = new AreaModel;
    	$a = $area_model->create($data);

    	if($a){
    		\Session::flash('success','Area Added Successfully');
    		return Redirect::to('transmittal/area');
    	}
    }

    public function updatearea(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);

    	$a = AreaModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Area Updated Successfully');
    		return Redirect::to('transmittal/area');
    	}
    }

    public function removearea($id=''){
    	$a = AreaModel::find($id)->delete();
    	if($a){
    		\Session::flash('success','Area Deleted Successfully');
    		return Redirect::to('transmittal/area');
    	}
    }
    public function branch(){
    	$branch_list = BranchModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.branch',compact('branch_list'));
    }
    public function savebranch(Request $request){
    	$data = $request->all();
    	$branch_model = new BranchModel;
    	$a = $branch_model->create($data);
    	if($a){
    		\Session::flash('success','Branch Added Successfully');
    		return Redirect::to('transmittal/branch');
    	}
    }
    public function updatebranch(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);
    	$a = BranchModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Branch Updated Successfully');
    		return Redirect::to('transmittal/branch');
    	}
    }
    public function removebranch($id=''){
    	$a = BranchModel::find($id)->delete();
    	if($a){
    		\Session::flash('success','Branch Deleted Successfully');
    		return Redirect::to('transmittal/branch');
    	}
    }
    public function branchtype(){
    	$branchtype_list = BranchTypeModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.branchtype',compact('branchtype_list'));
    }
    public function savebranchtype(Request $request){
    	$data = $request->all();
    	$branhctype_model = new BranchTypeModel;
    	$a = $branhctype_model->create($data);
    	if($a){
    		\Session::flash('success','Branch Type Added Successfully');
    		return Redirect::to('transmittal/branchtype');
    	}
    }
    public function updatebranchtype(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);

    	$a = BranchTypeModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Branch Type Updated Successfully');
    		return Redirect::to('transmittal/branchtype');
    	}
    }
    public function removebranchtype($id=''){
    	$a = BranchTypeModel::find($id)->delete();
    	if ($a) {
    		\Session::flash('success','Branch Type Deleted Successfully');
    		return Redirect::to('transmittal/branchtype');
    	}
    }
    public function ChronoType(){
    	$ChronoType_list = ChronoTypeModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.chronotype',compact('ChronoType_list'));
    }

    public function saveChronoType(Request $request){
    	$data = $request->all();
    	$ChronoType_model = new ChronoTypeModel;
    	$a = $ChronoType_model->create($data);
    	if($a){
    		\Session::flash('success','Chrono Type Added Successfully');
    		return Redirect::to('transmittal/chronotype');
    	}
    }
    public function updateChronoType(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);
    	$a = ChronoTypeModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Chrono Type Updated Successfully');
    		return Redirect::to('transmittal/chronotype');
    	}
    }
    public function removeChronoType($id=''){
    	$a = ChronoTypeModel::find($id)->delete();
    	if($a){
    		\Session::flash('success','Chrono Type Deleted Successfully');
    		return Redirect::to('transmittal/chronotype');
    	}
    }
    public function Promo(){
    	$promo_list = PromoModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.promo',compact('promo_list'));
    }
    public function savePromo(Request $request){
    	$data = $request->all();
    	$promo_model = new PromoModel;
    	$a = $promo_model->create($data);
    	if($a){
    		\Session::flash('success','Promo Added Successfully');
    		return Redirect::to('transmittal/promo');
    	}
    }
    public function updatePromo(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);
    	$a = PromoModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Promo Updated Successfully');
    		return Redirect::to('transmittal/promo');
    	}
    }
    public function removePromo($id=''){
    	$a = PromoModel::find($id)->delete();
    	if($a){
    		\Session::flash('success','Promo Deleted Successfully');
    		return Redirect::to('transmittal/promo');
    	}
    }

    public function transmittal(){
    	$transinfo = new TransmittalModel;
    	$search = \Request::get('search');
    	$trans_info = $transinfo->TransInfoDisplay($search);
    	return view('admin.transmittal.transmittal_info',compact('trans_info'));
    }
    public function addTransInfo(){
    	$area_info = AreaModel::all();
    	$branch_info = BranchModel::all();
    	$branchtype_info = BranchTypeModel::all();
    	$chrono_info = ChronoTypeModel::all();
    	$promo_info = PromoModel::all();
    	$trans_info = TransmittalModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.add_transinfo',compact('trans_info','area_info','branch_info','branchtype_info','chrono_info','promo_info'));
    }
    public function saveTransInfo(Request $request){
    	$data = $request->all();
    	$trans_model = new TransmittalModel;
    	$a = $trans_model->create($data);
    	if($a){
    		\Session::flash('success','Transmittal Information Added Successfully');
    		return Redirect::to('transmittal/transmittal_info');
    	}
    }
    public function editTransInfo($id=''){
    	$item = TransmittalModel::findOrFail($id);
    	$area_info = AreaModel::all();
    	$branch_info = BranchModel::all();
    	$branchtype_info = BranchTypeModel::all();
    	$chrono_info = ChronoTypeModel::all();
    	$promo_info = PromoModel::all();
    	$trans_info = TransmittalModel::orderBy('id','desc')->get();
    	return view('admin.transmittal.edit_transinfo',compact('trans_info','item','area_info','branch_info','branchtype_info','chrono_info','promo_info'));
    }
    public function updateTransInfo(Request $request){
    	$data = $request->all();
    	$data = request()->except(['_token']);
    	$a = TransmittalModel::where('id',$data['id'])->update($data);
    	if($a){
    		\Session::flash('success','Transmittal Information Updated Successfully');
    		return Redirect::to('transmittal/transmittal_info');
    	}
    }
    public function removeTransInfo($id=''){
    	$a = TransmittalModel::findOrFail($id)->delete();
    	if($a){
    		\Session::flash('success','Transmittal Information Deleted Successfully');
    		return Redirect::to('transmittal/transmittal_info');
    	}
    }
    public function printPreview(){
    	$transinfo = new TransmittalModel;
    	$trans_info = $transinfo->TransInfoDisplay();
    	return view('admin.transmittal.print_preview',compact('trans_info'));
    }

}
