<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\UseraccountModel;
use Redirect;
use Session;

class LoginController extends Controller
{
    public function index(){
    	if(Session::has('isLogin')){
    		return Redirect::to('/dashboard');
    	}else{
    		return view('admin.login.login');
    	}
    }

    public function loginAuth(Request $request){

    	$data = $request->all();

    	$authData = UseraccountModel::where('username',$data['email'])->where('password',$data['pass'])->first();
    	if($authData){
    		session()->put('isLogin',$authData->id);
    		return Redirect::to('/dashboard');
    	}else{
    		Session::flash('Warning',"Username or Password doesn't match");
    		return Redirect::to('/login');
    	}
    }

    public function logout(){
    	Session::forget('isLogin');
    	return redirect('/login');
    }
}
