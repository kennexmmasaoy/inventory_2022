<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Model\watchinfomodel;

use App\Http\Requests;
use Session;
use Redirect;

class AdminController extends Controller
{
    

    public function index(){
    	$watchinfo = new watchinfomodel;
    	$info_list = $watchinfo->watchinfodisplay();

    	if(Session::has('isLogin')){
      		return view('admin.productspecs.watch_info',compact('info_list'));
      	}else{
      		return Redirect::to('/login');
      	}
		
	}
	//for products
	public function productmanagement(){
		if(Session::has('isLogin')){
			return view('admin.products.product_management');
		}else{
      		return Redirect::to('/login');
      	}
	}
}
