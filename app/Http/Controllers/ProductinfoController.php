<?php

namespace App\Http\Controllers;
use Redirect;
use DB;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Model\ProductinfoModel;
use App\Http\Model\CasematerialModel;
use App\Http\Model\CasethicknessModel;
use App\Http\Model\CasewidthModel;
use App\Http\Model\ClasptypeModel;
use App\Http\Model\ColorModel;
use App\Http\Model\FaceheightModel;
use App\Http\Model\WristcircumferenceModel;
use App\Http\Model\WaterresistanceModel;
use App\Http\Model\WatchinfoModel;
use App\Http\Model\DialModel;
use App\Http\Model\ImageModel;
use App\Http\Model\ZipModel;
use App\Http\Model\BrandModel;

class ProductinfoController extends Controller
{
    //for product band materials
	public function bandmaterials(){
        if(Session::has('isLogin')){
		    $band_list = ProductinfoModel::orderBy('id', 'desc')->get();
		    return view('admin.productspecs.band_materials',compact('band_list'));
        }else{
            return view('admin.login.login');
        }
	}
	public function savebandmaterials(Request $request){
		$data = $request->all();
		$band_model = new ProductinfoModel;
		$b = $band_model->create($data);

		if($b)
		{
			\Session::flash('success','Band Material Added Successfully');
			return Redirect::to('productspecs/band_materials');
		}
	}
	public function updatebandMaterial(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = ProductinfoModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Band material Updated Successfully');
            return Redirect::to('productspecs/band_materials');
        }
    }
    public function removebandMaterial($id=''){

        $b = ProductinfoModel::find($id)->delete();

        if($b){
             \Session::flash('success','Band material Deleted Successfully');
            return Redirect::to('productspecs/band_materials');
        }
    }
    //for product brand
    public function brand(){
            $brand_list = BrandModel::orderBy('id', 'desc')->get();
            return view('admin.productspecs.brand',compact('brand_list'));
    }
    public function savebrand(Request $request){
        $data = $request->all();
        $band_model = new BrandModel;
        $b = $band_model->create($data);

        if($b)
        {
            \Session::flash('success','Brand Added Successfully');
            return Redirect::to('productspecs/brand');
        }
    }
    public function updatebrand(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = BrandModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Brand Updated Successfully');
            return Redirect::to('productspecs/brand');
        }
    }
    public function removebrand($id=''){

        $b = BrandModel::find($id)->delete();

        if($b){
             \Session::flash('success','Brand Deleted Successfully');
            return Redirect::to('productspecs/brand');
        }
    }


    //for case materials
	public function casematerials(){
		$case_list = CasematerialModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.case_materials',compact('case_list'));
	}
	public function savecasematerials(Request $request){
		$data = $request->all();
		$case_model = new CasematerialModel;
		$b = $case_model->create($data);

		if($b)
		{
			\Session::flash('success','Case Material Added Successfully');
			return Redirect::to('productspecs/case_materials');
		}
	}
	public function updatecaseMaterial(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = CasematerialModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Case material Updated Successfully');
            return Redirect::to('productspecs/case_materials');
        }
    }
    public function removecaseMaterial($id=''){

        $b = CasematerialModel::find($id)->delete();

        if($b){
             \Session::flash('success','Case material Deleted Successfully');
            return Redirect::to('productspecs/case_materials');
        }
    }


    //for case thickness
	public function casethickness(){
		$thick_list = CasethicknessModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.case_thickness',compact('thick_list'));
	}
	public function savecasethickness(Request $request){
		$data = $request->all();
		$thick_model = new CasethicknessModel;
		$b = $thick_model->create($data);

		if($b)
		{
			\Session::flash('success','Case Thickness Added Successfully');
			return Redirect::to('productspecs/case_thickness');
		}
	}
	public function updatecasethickness(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = CasethicknessModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Case Thickness Updated Successfully');
            return Redirect::to('productspecs/case_thickness');
        }
    }
    public function removecasethickness($id=''){

        $b = CasethicknessModel::find($id)->delete();

        if($b){
             \Session::flash('success','Case Thickness Deleted Successfully');
            return Redirect::to('productspecs/case_thickness');
        }
    }

    //for case width
	public function casewidth(){
		$width_list = CasewidthModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.case_width',compact('width_list'));
	}
	public function savecasewidth(Request $request){
		$data = $request->all();
		$width_model = new CasewidthModel;
		$b = $width_model->create($data);

		if($b)
		{
			\Session::flash('success','Case Width Added Successfully');
			return Redirect::to('productspecs/case_width');
		}
	}
	public function updatecasewidth(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = CasewidthModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Case Width Updated Successfully');
            return Redirect::to('productspecs/case_width');
        }
    }
    public function removecasewidth($id=''){

        $b = CasewidthModel::find($id)->delete();

        if($b){
             \Session::flash('success','Case Width Deleted Successfully');
            return Redirect::to('productspecs/case_width');
        }
    }



    //for clasp type
	public function clasptype(){
		$clasp_list = ClasptypeModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.clasp_type',compact('clasp_list'));
	}

	public function saveclasptype(Request $request){
		$data = $request->all();
		$clasp_model = new ClasptypeModel;
		$b = $clasp_model->create($data);

		if($b)
		{
			\Session::flash('success','Clasp Type Added Successfully');
			return Redirect::to('productspecs/clasp_type');
		}
	}
	public function updateclasptype(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = ClasptypeModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Clasp Type Updated Successfully');
            return Redirect::to('productspecs/clasp_type');
        }
    }
    public function removeclasptype($id=''){

        $b = ClasptypeModel::find($id)->delete();

        if($b){
             \Session::flash('success','Clasp Type Deleted Successfully');
            return Redirect::to('productspecs/clasp_type');
        }
    }

    //for color
	public function color(){
		$color_list = ColorModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.color',compact('color_list'));
	}

	public function savecolor(Request $request){
		$data = $request->all();
		$color_model = new ColorModel;
		$b = $color_model->create($data);

		if($b)
		{
			\Session::flash('success','Color Added Successfully');
			return Redirect::to('productspecs/color');
		}
	}
	public function updatecolor(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = ColorModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Color Updated Successfully');
            return Redirect::to('productspecs/color');
        }
    }
    public function removecolor($id=''){

        $b = ColorModel::find($id)->delete();

        if($b){
             \Session::flash('success','Color Deleted Successfully');
            return Redirect::to('productspecs/color');
        }
    }

    //for face height
	public function faceheight(){
		$face_list = FaceheightModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.face_height',compact('face_list'));
	}
	public function savefaceheight(Request $request){
		$data = $request->all();
		$face_model = new FaceheightModel;
		$b = $face_model->create($data);

		if($b)
		{
			\Session::flash('success','Face Height Added Successfully');
			return Redirect::to('productspecs/face_height');
		}
	}
	public function updatefaceheight(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = FaceheightModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Face Height Updated Successfully');
            return Redirect::to('productspecs/face_height');
        }
    }
    public function removefaceheight($id=''){

        $b = FaceheightModel::find($id)->delete();

        if($b){
             \Session::flash('success','Face Height Deleted Successfully');
            return Redirect::to('productspecs/face_height');
        }
    }

    //for wrist circunference
    public function wristcircumference(){
    	$wrist_list = WristcircumferenceModel::orderBy('id', 'desc')->get();
		return view('admin.productspecs.wrist_circumference',compact('wrist_list'));
	}

	public function savewristcircumference(Request $request){
		$data = $request->all();
		$wrist_model = new WristcircumferenceModel;
		$b = $wrist_model->create($data);

		if($b)
		{
			\Session::flash('success','Max Wrist Circumference Added Successfully');
			return Redirect::to('productspecs/wrist_circumference');
		}
	}
	public function updatewristcircumference(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = WristcircumferenceModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Max Wrist Circumference Updated Successfully');
            return Redirect::to('productspecs/wrist_circumference');
        }
    }
    public function removewristcircumference($id=''){

        $b = WristcircumferenceModel::find($id)->delete();

        if($b){
             \Session::flash('success','Max Wrist Circumference Deleted Successfully');
            return Redirect::to('productspecs/wrist_circumference');
        }
    }

    //for water resistance
    public function waterresistance(){
    	$water_list = WaterresistanceModel::orderBy('id','desc')->get();
		return view('admin.productspecs.water_resistance',compact('water_list'));
	}
	public function savewaterresistance(Request $request){
		$data = $request->all();
		$water_model = new WaterresistanceModel;
		$b = $water_model->create($data);

		if($b)
		{
			\Session::flash('success','Water Resistance Added Successfully');
			return Redirect::to('productspecs/water_resistance');
		}
	}
	public function updatewaterresistance(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = WaterresistanceModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Water Resistance Updated Successfully');
            return Redirect::to('productspecs/water_resistance');
        }
    }
    public function removewaterresistance($id=''){

        $b = WaterresistanceModel::find($id)->delete();

        if($b){
             \Session::flash('success','Water Resistance Deleted Successfully');
            return Redirect::to('productspecs/water_resistance');
        }
    }

    //for dial
    public function dial(){
        $dial_list = DialModel::orderBy('id','desc')->get();
        return view('admin.productspecs.dial',compact('dial_list'));
    }
    public function savedial(Request $request){
        $data = $request->all();
        $water_model = new DialModel;
        $b = $water_model->create($data);

        if($b)
        {
            \Session::flash('success','Dial Added Successfully');
            return Redirect::to('productspecs/dial');
        }
    }
    public function updatedial(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = DialModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','Dial Updated Successfully');
            return Redirect::to('productspecs/dial');
        }
    }
    public function removedial($id=''){

        $b = DialModel::find($id)->delete();

        if($b){
             \Session::flash('success','Dial Deleted Successfully');
            return Redirect::to('productspecs/dial');
        }
    }



    //for watch info
    public function watchinfo(){
        $watchinfo = new WatchinfoModel;
        $search = \Request::get('search');

        $info_list = $watchinfo->watchinfoDisplay($search);
        

		return view('admin.productspecs.watch_info',compact('info_list'));
	}
     public function addwatchinfo(){
        $color_name = ColorModel::all();
        $brand = BrandModel::all();
        $case_material = CasematerialModel::all();
        $case_thickness = CasethicknessModel::all();
        $case_width = CasewidthModel::all();
        $face_height = FaceheightModel::all();
        $max_wrist_circumference = WristcircumferenceModel::all();
        $water_resistance = WaterresistanceModel::all();
        $clasp_type = ClasptypeModel::all();
        $dial = DialModel::all();
        $info_list = WatchinfoModel::orderBy('id','desc')->get();
        return view('admin.productspecs.add_watchinfo',compact('info_list','color_name','case_material','case_thickness','brand','case_width','face_height','max_wrist_circumference','water_resistance','clasp_type','dial'));
    }
     public function editwatchinfo($id=''){
        $item =  WatchinfoModel::findOrFail($id);

        $getWatchImage = ImageModel::where('id',$item->image_id)->first();

        $color_name = ColorModel::all();
        $brand = BrandModel::all();
        $case_material = CasematerialModel::all();
        $case_thickness = CasethicknessModel::all();
        $case_width = CasewidthModel::all();
        $face_height = FaceheightModel::all();
        $max_wrist_circumference = WristcircumferenceModel::all();
        $water_resistance = WaterresistanceModel::all();
        $clasp_type = ClasptypeModel::all();
        $dial = DialModel::all();
        $info_list = WatchinfoModel::orderBy('id','desc')->get();
        return view('admin.productspecs.edit_watchinfo',compact('getWatchZip','item','info_list','color_name','brand','case_material','case_thickness','case_width','face_height','max_wrist_circumference','water_resistance','clasp_type','dial'));
    }
	public function savewatchinfo(Request $request){
		$data = $request->all();
        $img_model = new ImageModel;
        $zip_model = new ZipModel;

        //get file
        $image      = $request -> file('image');

        //get the name of the file
        $real_name  = $image -> getClientOriginalName();
        //convert the file name to encrypted 
        $input      = md5($real_name).time().'.'.$image->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/');
        $image ->move($destinationPath, $input);
        
        $pid = $img_model ->insertImg($data['sku'],$input,$real_name,$image);
        $data['image_id'] = $pid;


        $info_model = new WatchinfoModel;
		$b = $info_model->create($data);

        //get zip file
        $zip        = $request -> file('zip');

        $real_name  = $zip -> getClientOriginalName();
        $size       = $zip -> getsize();
        //convert the file name to encrypted 
        $input      = md5($real_name).time().'.'.$zip->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/zip');
        $zip ->move($destinationPath, $input);
        $channel = 0;

        $zid = $zip_model ->insertZip($size,$input,$real_name,$b['id'],$channel);
        //get zip1 file
        $zip1        = $request -> file('zip1');

        $real_name1  = $zip1 -> getClientOriginalName();
        $size1       = $zip1 -> getsize();
        //convert the file name to encrypted 
        $input1      = md5($real_name1).time().'.'.$zip1->getCLientOriginalExtension();
        $channel1 = 1;  
        
        $destinationPath = public_path('/upload/zip');
        $zip1 ->move($destinationPath, $input1);
        
        $zid1 = $zip_model ->insertZip($size1,$input1,$real_name1,$b['id'],$channel1);

        //get zip2 file
        $zip2       = $request -> file('zip2');

        $real_name2  = $zip2 -> getClientOriginalName();
        $size2       = $zip2 -> getsize();
        //convert the file name to encrypted 
        $input2      = md5($real_name2).time().'.'.$zip2->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/zip');
        $zip2 ->move($destinationPath, $input2);

        $channel2 = 2;
        $zid2 = $zip_model ->insertZip($size2,$input2,$real_name2,$b['id'],$channel2);
        

        //get zip3 file
        $zip3        = $request -> file('zip3');

        $real_name3  = $zip3 -> getClientOriginalName();
        $size3       = $zip3 -> getsize();
        //convert the file name to encrypted 
        $input3      = md5($real_name3).time().'.'.$zip3->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/zip');
        $zip3 ->move($destinationPath, $input3);

        $channel3 = 3;
        $zid3 = $zip_model ->insertZip($size3,$input3,$real_name3,$b['id'],$channel3);
        

		if($b)
		{
			\Session::flash('success','Watch Information Added Successfully');
			return Redirect::to('productspecs/watch_info');
		}
	}

	public function updateWatchInfo(Request $request){
        $data = $request->all();
        
        if(!empty($request->image)){

        $img_model = new ImageModel;
        //get file
        $image      = $request -> file('image');
        //get the name of the file
        $real_name  = $image -> getClientOriginalName();
        //convert the file name to encrypted 
        $input      = md5($real_name).time().'.'.$image->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/');
        $image ->move($destinationPath, $input);
        
        $pid = $img_model ->insertImg($data['sku'],$input,$real_name,$image);
        $data['image_id'] = $pid;

        $gim = new WatchinfoModel;
        $goodsImageData = array(
                'id' => $request->id,
                'image_id' => $pid,
            );
            $test = $gim->where('id',$request->id)->update($goodsImageData);
    }
        $info_model = new WatchinfoModel;
        $b = $info_model->update($data);

        if(!empty($request->zip)){

        $zip_model = new ZipModel;
        //get zip file
        $zip        = $request -> file('zip');

        $real_name  = $zip -> getClientOriginalName();
        $size       = $zip -> getsize();
        //convert the file name to encrypted 
        $input      = md5($real_name).time().'.'.$zip->getCLientOriginalExtension();  
        
        $destinationPath = public_path('/upload/zip');
        $zip ->move($destinationPath, $input);
        $channel = 0;
        $zid = $zip_model ->updateZip($size,$input,$real_name,$b['id'],$channel);
        $zim = new WatchinfoModel;
        $goodsZipData = array(
                'id' => $request->id,
            );
            $test = $zim->where('id',$request->id)->update($goodsZipData);
    }

        $data = $request->except('_token','image');
        $b = WatchinfoModel::findOrFail($request->id)->update($data);

        if($b){
             \Session::flash('success','Watch Information Updated Successfully');
            return Redirect::to('/productspecs/watch_info');
        }
    }
    public function removewatchinfo($id=''){

        $b = WatchinfoModel::findOrFail($id)->delete();

        if($b){
             \Session::flash('success','Watch Information Deleted Successfully');
            return Redirect::to('productspecs/watch_info');
        }
    }
	public function zipupload(){
		return view('admin.productspecs.zip_upload');
	}
    public function uploadBulk(Request $request){
        $validator = Validator::make($request->all(),[
            'file' => 'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $file = $request->file('file');

        $csvData = file_get_contents($file);

        $rows = array_map('str_getcsv', explode("\n", $csvData));

        $headers = array_shift($rows);
       
        foreach($rows as $row){
            $row = array_combine($headers, $row);
            $data_bulk = array(
                'barcode' => $row['Barcode'],
                'sku' => $row['sku'],
                'price' => $row['Price'],
                'product_description' => $row['Description']
            );
            $check_product_if_available = WatchinfoModel::where('sku',$row['sku'])->where('barcode',$row['Barcode'])->first();
            
            if($check_product_if_available){
                WatchinfoModel::find($check_product_if_available->id)->update($data_bulk);
            }else{
                WatchinfoModel::insert( $data_bulk);
            }
        }
        
        // flash('All Products Uploaded / Updated');
        return redirect()->back();
    }

}
