<?php

namespace App\Http\Controllers;
use App\Http\Model\WatchinfoModel;
use App\Http\Model\DocumentModel;
use App\Http\Model\PlatformProductListModel;
use App\Http\Model\PlatformProductListHistoryModel;
use Khill\Lavacharts\Lavacharts;
use DB;
use Carbon;
use Session;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
    	$watchinfo = new WatchinfoModel;
    	$search = \Request::get('search');
        $info_list = $watchinfo->watchinfoDisplay($search);
		return view('homepage.index',compact('info_list'));
	}
	public function listview(){
		$watchinfo = new WatchinfoModel;
		$search = \Request::get('search');
        $info_list = $watchinfo->watchinfoDisplay($search);
		return view('homepage.listview',compact('info_list'));
	}
	public function register(){
		return view('homepage.registration');
	}
	public function signin(){
		return view('homepage.signin');
	}
	public function dashboard(){
		$lava = new Lavacharts;
		$transIn = DocumentModel::where('type','1')->where('status','>=','0')->limit(300)->orderBy('counter','desc')->get();
		$transOut = DocumentModel::where('type','0')->limit(300)->orderBy('counter','desc')->get();
		// dd($transOut);
		$itemCounter = PlatformProductListModel::groupBy('platform_id')
			->limit(5)
			->orderBy('totalQty','DESC')
			->get([DB::raw("SUM(finalQuantity) as totalQty"),'platform_id']);
		//calling the report method
		// self::areaChart($lava);
		return view('admin.dashboard.dashboard',compact('transIn','transOut','itemCounter'));
	}
	private static function areaChart($lava){
		$x='-1';
		$itemCount ='';
		$sales = $lava->Datatable();
		// $data = PlatformProductListHistoryModel::select('quantity','created_at')->where('type',0)->get()
		// ->groupBy(function($date) {
		//        return Carbon::parse($date->created_at)->format('Y-m-d'); // grouping by daily
		//    })
		//     ->toArray();
		$data = PlatformProductListHistoryModel::join('platform_product_list','platform_product_list.platform_product_list_id','=','platform_product_list_history.PPL_id')
			->join('watch_info','watch_info.barcode','=','platform_product_list.barcode')
			->select('platform_product_list_history.created_at as publishDate', 'platform_product_list_history.quantity as qty','watch_info.price as prc')
			->get()
			->groupBy(function($date) {
		        return Carbon::parse($date->publishDate)->format('Y-m-d'); // grouping by daily
		    })
			->toArray();
		$sales->addDateColumn('Date')->addNumberColumn('Total Number of Out');
		foreach($data as $key => $value ){
			// if($key==$key){
				$itemCount = ($value['qty'] * $value['prc']);
			// }
			$sales->addRow([
				$key,$itemCount,
			]);
		}
		$lava->AreaChart('Sales',$sales,[
			'title' => 'Number Daily of Sales for the last 7 Days',
			'legend'=>[
				'position'=>'in'
			]
		]);
	}
}
