<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\PlatformModel;
use App\Http\Model\DocumentModel;
use App\Http\Model\WatchInfoModel;
use App\Http\Model\PlatformProductListModel;
use App\Http\Model\PlatformProductListHistoryModel;
use Session;
use Input;
use Redirect;
use Carbon;
use Validator;

class InventoryController extends Controller
{
    public function transferIn($id){
        $heading = "Transfer In";
    	$transfer_type = 1;
        $status = '-1';
        $doc = self::createDocumentTransferIn($id, $transfer_type,$status,1);
    	$plat = PlatformModel::all();
    	$current_prod_history = PlatformProductListHistoryModel::
    		leftJoin('platform_product_list','platform_product_list.platform_product_list_id','=','platform_product_list_history.PPL_id')
    		->where('document_id',$doc)
    		->where('type','1')
    		->get();
    	return view('admin.inventory.index',compact('plat','doc','current_prod_history','transfer_type','heading'));
    }
    function createDocumentTransferIn($id,$transfer_type,$status='',$type){
        $todayDate = Carbon::now()->toDateString();
        if($id=='new'){
            $checkData = DocumentModel::where('type',$transfer_type)->orderBy('id','desc')->first();
            if($checkData){
                $checkData->id+=1;
                $data = array(
                    'id' => $checkData->id,
                    'user_id' => session()->get('isLogin'),
                    'status' => $status,
                    'selectedDate' => $todayDate,
                    'type' => $type,
                );
                $doc_id = DocumentModel::create($data);
                $doc = $checkData->id;
            }else{
                $data = array(
                    'id' => '1',
                    'user_id' => session()->get('isLogin'),
                    'status' => $status,
                    'selectedDate' => $todayDate,
                    'type' => $type,
                );
                $doc_id = DocumentModel::create($data);
                $doc = $data['id'];
            }
        }else{
            $doc = $id;
        }
        return $doc;
    }
    public function transferOut($id){
        $heading = "Transfer Out";
    	$transfer_type = 0;
        $status = '-1';
        $doc = self::createDocumentTransferIn($id, $transfer_type,$status,0);
    	$plat = PlatformModel::all();
    	$current_prod_history = PlatformProductListHistoryModel::
    		leftJoin('platform_product_list','platform_product_list.platform_product_list_id','=','platform_product_list_history.PPL_id')
    		->where('document_id',$doc)
    		->where('type','=','0')
    		->get();
    	return view('admin.inventory.index',compact('plat','doc','current_prod_history','transfer_type','heading'));
    }

    public function saveDocument(){
    	$current_date = Input::get('date');
    	$platform = Input::get('plat');
    	$quantity = Input::get('qty');
    	$sku = Input::get('sku');
    	$doc_id = Input::get('doc_id');
    	$trans_type = Input::get('trans_type');
        $sumPrice = 0;
        $pro = PlatformProductListModel::where('platform_id',$platform)
                    ->where(function($que) use ($sku){
                        $que->orWhere('barcode',$sku)->orWhere('sku',$sku);
                    });
        $prod_search_ppl = $pro->first();
        if($trans_type == 1){
           $prodTotalQuantity = $prod_search_ppl->finalQuantity + $quantity; 
        }else{
            $prodTotalQuantity = $prod_search_ppl->finalQuantity - $quantity;
        }
        /*
        * update product list final quantity
        */
        // $prodListData = array(
        //     'finalQuantity'=> $prodTotalQuantity
        // );
        // $update_prodList = $pro->update($prodListData);
    	 
    	$desc_search = WatchInfoModel::where('sku',$sku)->orWhere('barcode',$sku)->first();
    	$getPlat = PlatformModel::where('id',$platform)->first();

        $data_prod_list = array(
            'document_id' => $doc_id,
            'PPL_id' =>  $prod_search_ppl->platform_product_list_id,
            'quantity' => $quantity,
            'totalQuantity' => $prodTotalQuantity,
            'type' => $trans_type
        );
    	$create_prod_history = PlatformProductListHistoryModel::create($data_prod_list);
        if($create_prod_history){
            $getDocInfo = PlatformProductListHistoryModel::where('document_id',$doc_id)->where('type',$trans_type);
            //sum all items on specific type and document
            $sumQuantity = $getDocInfo->sum('quantity');
            $arItem = $getDocInfo->get();

            foreach($arItem as $key => $item){
                $ppl = PlatformProductListModel::where('platform_product_list_id',$item['PPL_id'])->first();
                $WatchInfoModel = WatchInfoModel::where('barcode',$ppl->barcode)->first();
                $sumPrice += $WatchInfoModel->price;
            }           

    		$ret = array(
    			'msg' => 'Successfully Added to Product List',
	    		'status' => 1,
                'history_id' => $create_prod_history->id,
	    		'barcode' => $prod_search_ppl->barcode,
	    		'sku' => $prod_search_ppl->sku,
	    		'qty' => $quantity,
	    		'platform' => $getPlat->platform_name,
	    		'desc' => $desc_search->product_description,
                'price' => $desc_search->price,
                'totalPrice' => $quantity * $desc_search->price,
                'finalQuantity' => $sumQuantity,
                'finalPrice' => $sumPrice,
                'finalTotalPrice' => ($sumQuantity * $sumPrice)
    		);
    	}else{
    		$ret = array(
    			'msg' => 'Failed in adding the item in the  Product List',
	    		'status' => 0
    		);
    	}
    	return \Response::json($ret);
    }

    public function searchValidationItem(){
    	$sku = Input::get('sku');
        $plat = Input::get('platform');
    	$watch_value = WatchInfoModel::where('barcode',$sku)->orWhere('sku',$sku)->first();
    	$searchWatchIfExist = PlatformProductListModel::where('platform_id',$plat)
    							->where(function($que) use($sku) {
					    		$que->orWhere('barcode',$sku)->orWhere('sku',$sku);
					    	})->first();
    	if($searchWatchIfExist){
            $ret = array(
                'msg' => 'Product Available',
                'status' => 1,
                'desc' =>  $watch_value->product_description,
            );
    	}else{
            $ret = array(
                'msg' => 'Product Cannot be found in the list',
                'status' => 0,
                'desc' =>  'no description',
            );
    	}
        return $ret;
    }
    public function addToProductList(){
    	$sku = Input::get('sku');
    	$plat_id = Input::get('platform');
    	$doc_id = Input::get('document_id');
        $trans_type = Input::get('trans_type');

    	$watch_value = WatchInfoModel::where('barcode',$sku)->orWhere('sku',$sku)->first();
    	if($watch_value){
    		$dataList= array(
    		'sku' => $watch_value->sku,
    		'platform_id' => $plat_id,
    		'barcode' => $watch_value->barcode
	    	);
	    	$added_prod = PlatformProductListModel::create($dataList);

	    	$doc_log_data = array(
	    		'document_id' => $doc_id,
	    		'PPL_id' => $added_prod->id,
	    		'type' => '-1'
	    	);

	    	$doc_log = PlatformProductListHistoryModel::create($doc_log_data);

	    	$ret = array(
	    		'msg' => 'Successfully Added to Product List',
	    		'id' => $added_prod->id,
	    		'status' => 1
	    	);
	    	return \Response::json($ret);
    	}else{
    		$ret = array(
	    		'msg' => 'Product Not Found in the General List',
	    		'id' => 0,
	    		'status' => 0
	    	);
	    	return \Response::json($ret);
    	}
    }
    public function saveDraft(){
    	$doc_id = Input::get('doc_id');
    	$trans_type = Input::get('trans_type');
    	$doc_data = array(
    		'status' => 0,
    		'type' => $trans_type
    	);
    	$i = DocumentModel::where('id',$doc_id)->where('type',$trans_type)->update($doc_data);
    	if($i){
    		$ret = array(
	    		'msg' => 'Successfully save document',
	    		'status' => 1
	    	);
    	}else{
    		$ret = array(
	    		'msg' => 'Failed to save. Please Try again',
	    		'status' => 0
	    	);
    	}
    	return \Response::json($ret);
    }
    public function postDocument(){
    	$doc_id = Input::get('doc_id');
    	$trans_type = Input::get('trans_type');
    	$doc_data = array(
    		'status' => 1,
    	);
        $arr = PlatformProductListHistoryModel::where('document_id',$doc_id)->where('type',$trans_type)->get();
        foreach($arr as $key =>$data){
            $search_prod = PlatformProductListModel::where('platform_product_list_id',$data['PPL_id'])->first();
            if($data['type'] == 1){
                $prod = array(
                    'finalQuantity'=> $data['quantity'] + $search_prod['finalQuantity'],
                );
            }else{
                $prod = array(
                    'finalQuantity'=> ($search_prod['finalQuantity'] - $data['quantity']),
                );
            }
            PlatformProductListModel::where('platform_product_list_id',$data['PPL_id'])->update($prod);
        }
    	$i = DocumentModel::where('id',$doc_id)->where('type',$trans_type)->update($doc_data);
    	if($arr){
    		$ret = array(
    			'msg' => 'Successfully posted document',
    			'status' => 1
    		);
    	}else{
    		$ret = array(
    			'msg' => 'Opps! Something Went Wrong.',
    			'status' => 0
    		);
    	}
    	return \Response::json($ret);
    }
    public function deleteDocumentRecord(){
    	$doc_id = Input::get('doc_id');
    	$trans_type = Input::get('trans_type');

    	dd($trans_type);
    	$i = DocumentModel::where('id',$doc_id)->where('type',$trans_type)->orderBy('counter','desc')->delete();
    	if($i){
	    	$ret = array(
	    		'msg' => 'The document is deleted',
	    		'status' => 1
	    	);
    	}else{
    		$ret = array(
	    		'msg' => 'The document is not deleted',
	    		'status' => 0
	    	);
    	}
    	return \Response::json($ret);
    }
    public function checkDocument(){
        $doc_id = Input::get('doc_id');
        $trans_type = Input::get('trans_type');
        $i = DocumentModel::where('id',$doc_id)->where('type',$trans_type)->orderBy('counter','desc')->first();
        if($i){
            $ret = array(
                'msg' => 'found document',
                'status' => $i->status
            );
        }else{
            $ret = array(
                'msg' => 'missing document',
                'status' => -2
            );
        }
        return \Response::json($ret);
    }
    public function removeItemList(){
        $id = Input::get("id");
        $i = PlatformProductListHistoryModel::find($id)->delete();
        $ret = array(
            'msg' => "Product remove from the list",
            'status' => 1
        );
        return \Response::json($ret);
    }

    public function editItemList(){
        $id = Input::get("history_item_id");
        $qty = Input::get("product_quantity");
        $data = array(
            'quantity' => $qty
        );
        $i = PlatformProductListHistoryModel::find($id)->update($data);
        if($i){
            $ret = array(
                'msg' => 'Successfully Updated Product Quantity',
                'status' => 1
            );
            return \Response::json($ret);
        }
    }

    public function updateDocumentDate(){
         $dates = Input::get("dates");
         $id = Input::get("id");
         $trans_type = Input::get("trans_type");
         $ar = array(
            'selectedDate' => $dates
         );
         $i = DocumentModel::where('id',$id)->where('type',$trans_type)->update($ar);
         if($i){
            $ret = array(
                'msg' => "Successfully Updated Document Date!",
                'status' => 1
            );
         }else{
            $ret = array(
                'msg' => "Failed to Updated Document Date!",
                'status' => 0
            );
         }
         return \Response::json($ret);
    }

    public function bulkUploadIn(){
        return view('admin.inventory.bulk-upload');
    }

    public function postBulkUploadIn(Request $request){
        $data = $request->all();
        $id = 'new';
        $platform_id = PlatformModel::where('platform_name',$data['platform'])->select('id')->first();
        $doc_id = self::createDocumentTransferIn($id,1,1,1);

        $validator = Validator::make($request->all(),[
            'file' => 'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $file = $request->file('file');
        
        $csvData = file_get_contents($file);

        $rows = array_map('str_getcsv', explode("\n", $csvData));

        $headers = array_shift($rows);
        
        foreach($rows as $key => $row){
            $row = array_combine($headers, $row);
            $data_bulk = array(
                'platform_id' => $platform_id->id,
                'barcode' => $row['Barcode'],
                'sku' => $row['SKU'],
                'finalQuantity' => $row['Quantity'],
            );
            $ppl_model = PlatformProductListModel::where('platform_id',$platform_id->id)->where('barcode',$row['Barcode']);
            $check_product_if_available = $ppl_model->first();
            if($check_product_if_available){
                $data_bulk_update = array(
                    'finalQuantity' => $check_product_if_available->finalQuantity += $row['Quantity']
                );
                $ppl_model->update($data_bulk_update);
            }else{
                PlatformProductListModel::create($data_bulk);
            }  
            $ppl_id = $ppl_model->first();
            $data_ppl_history = array(
                'document_id' => $doc_id,
                'PPL_id' => $ppl_id->platform_product_list_id,
                'quantity' => $row['Quantity'],
                'type' => 1,
            );
            PlatformProductListHistoryModel::create($data_ppl_history);
        }
        Session::flash('success', 'Transfer In document Successfully executed!'); 
        return view('admin.inventory.bulk-upload');
    }

}
