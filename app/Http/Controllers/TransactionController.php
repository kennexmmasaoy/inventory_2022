<?php

namespace App\Http\Controllers;

use Redirect;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Http\Model\PlatformModel;
use App\Http\Model\TransactionModel;
use App\Http\Model\UseraccountModel;
use App\Http\Model\ImageModel;
use App\Http\Model\WatchlistModel;

class TransactionController extends Controller
{
    public function platform(){
        $plat_list = PlatformModel::all();
    	return view('admin.transaction.platform',compact('plat_list'));
    }
    public function saveplatform(Request $request){
    	$data = $request->all();
    	$plat_model = new PlatformModel;
    	$a = $plat_model->create($data);
    	if($a){
    		\Session::flash('success','Platform Added Successfully');
    		return Redirect::to('transaction/platform');
    	}
    }
    public function updateplatform(Request $request){
    	$data = $request->all();
    	$a = PlatformModel::findOrFail($request->id)->update($data);
    	if($a){
    		\Session::flash('success','Platform Updated Successfully');
    		return Redirect::to('transaction/platform');
    	}
    }
    public function removeplatform($id=''){
    	$a = PlatformModel::find($id)->delete();
    	if($a){
    		\Session::flash('success','Platform Deleted Successfully');
    		return Redirect::to('transaction/platform');
    	}
    }
    public function product_transaction(){
    	$plat_info = PlatformModel::all();
    	$user_info = UseraccountModel::all();
    	$transacinfo = new TransactionModel;
    	$transac_info = $transacinfo->transactionDisplay();
    	
    	return view('admin.transaction.product_transaction',compact('transac_info','plat_info','user_info'));
    }
    public function savetransaction(Request $request){
    	$data = $request->all();
    	$plat_info = PlatformModel::all();
    	$user_info = UseraccountModel::all();
    	$transac_model = new TransactionModel;
    	$data['uid'] = Session::get('isLogin');
    	$a = $transac_model->create($data);
    	if($a){
    		\Session::flash('success','Transaction Added Successfully');
    		return Redirect::to('transaction/product_transaction');
    	}
    }
    public function watchlist(){
    	$watchlist_info = WatchlistModel::orderBy('id', 'desc')->get();
		return view('admin.transaction.watchlist',compact('watchlist_info'));
    }
}
