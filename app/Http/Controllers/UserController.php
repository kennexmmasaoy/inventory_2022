<?php

namespace App\Http\Controllers;
use Redirect;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Http\Model\UserModel;
use App\Http\Model\UseraccountModel;
use App\Http\Model\UserprivilegesModel;

class UserController extends Controller
{
	//for user office info
    public function userofficeinfo(){
        
            $useroffice_list = UserModel::orderBy('id','desc')->get();
        if(Session::has('isLogin')){
        	return view('admin.user.userofficeinfo',compact('useroffice_list'));
        }else{
            return view('admin.login.login');
        }

    }
    //for user office info
    public function saveuserofficeinfo(Request $request){
    	$data = $request->all();
        if($request->status == 'on'){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
    	$office_model = new UserModel;
    	$b = $office_model->create($data);

    	if($b){
    		\Session::flash('success','User Office Information Added Successfully');
			return Redirect::to('user/userofficeinfo');
    	}
    }
    //for user office info
    public function updateuserofficeinfo(Request $request){
    	$data = $request->all();
        if($request->status == 'on'){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
    	$data = request()->except(['_token']);

    	$b = UserModel::where('id',$data['id'])->update($data);

    	if($b){
             \Session::flash('success','User Office Information Updated Successfully');
            return Redirect::to('user/userofficeinfo');
        }
    }
    //for user office info
    public function removeuserofficeinfo($id=''){

        $b = UserModel::find($id)->delete();

        if($b){
             \Session::flash('success','User Office Information Deleted Successfully');
            return Redirect::to('user/userofficeinfo');
        }
    }

    //for user account
    public function useraccount(){
        $Uaccount = new UseraccountModel;
    	$account_list = $Uaccount->userprivdisplay();
        $userpriv = UserprivilegesModel::all();
    	return view('admin.user.useraccount',compact('account_list','userpriv'));

    }
    //for user account save
    public function saveuseraccount(Request $request){
        $data = $request->all();
        $account_model = new UseraccountModel;
        $b = $account_model->create($data);

        if($b){
            \Session::flash('success','User Account Added Successfully');
            return Redirect::to('user/useraccount');
        }
    }
    //for user account update
     public function updateuseraccount(Request $request){
        $data = $request->all();
        $data = request()->except(['_token']);

        $b = UseraccountModel::where('id',$data['id'])->update($data);

        if($b){
             \Session::flash('success','User Account Updated Successfully');
            return Redirect::to('user/useraccount');
        }
        else{
            dd('error');
        }
    }
    //for user account delete
    public function removeuseraccount($id=''){

        $b = UseraccountModel::findOrFail($id)->delete();

        if($b){
             \Session::flash('success','User Account Deleted Successfully');
            return Redirect::to('user/useraccount');
        }
    }

    //for user privileges
    public function userpriv(){
    	$userpriv_list = UserprivilegesModel::orderBy('id','desc')->get();
    	return view('admin.user.userpriviliges',compact('userpriv_list'));

    }
    //for user priviliges
    public function saveuserpriviliges(Request $request){
    	$data = $request->all();
        if($request->status == 'on'){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }

    	$priv_model = new UserprivilegesModel;
    	$b = $priv_model->create($data);

    	if($b){
    		\Session::flash('success','User Privilage Added Successfully');
			return Redirect::to('user/userpriviliges');
    	}
    }
    //for user priviliges
    public function updateuserpriviliges(Request $request){
    	$data = $request->all();
        if($request->status == 'on'){
            $data['status'] = 1;
        }else{
            $data['status'] = 0;
        }
    	$data = request()->except(['_token']);

    	$b = UserprivilegesModel::where('id',$data['id'])->update($data);

    	if($b){
             \Session::flash('success','User Privilage Updated Successfully');
            return Redirect::to('user/userpriviliges');
        }
    }
    //for user priviliges
    public function removeuserpriviliges($id=''){

        $b = UserprivilegesModel::find($id)->delete();

        if($b){
             \Session::flash('success','User Privilages Deleted Successfully');
            return Redirect::to('user/userpriviliges');
        }
    }
}
